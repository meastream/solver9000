#!/bin/bash
# Переменные для профилировки (для обычного запуска не обращать внимания)
RESULT_DIR="stress_gpu_diagnostics"
COLLECT_OPTION="hotspots"

# Название исполняемого файла
SOLVER_FILE="./stress_solver_launcher_xeon_gpu"
# Количество процессов; По нему задается количество подмоделей (последним аргументом)
PROCESSES_NUM="2" 
# Размер стороны пластины
# SIDE_SIZE="50"
# SIDE_SIZE="100"
SIDE_SIZE="300"
# Количество субитераций
SUB_ITERATIONS="1"
# SUB_ITERATIONS="100"
# Тип решателя. Для запуска на GPU 6й тип проставляется в программе автоматически (см. main.cpp)
SOLVER="1"

# Запуск без профилировки
mpiexec -f machinefile -n $PROCESSES_NUM $SOLVER_FILE $SIDE_SIZE $SUB_ITERATIONS $SOLVER $PROCESSES_NUM 

# Профилировка
# rm -rf $RESULT_DIR*
# mpiexec -f machinefile -n $PROCESSES_NUM -l amplxe-cl -quiet -collect $COLLECT_OPTION -trace-mpi -result-dir $RESULT_DIR ./stress_solver_launcher $SIDE_SIZE $SUB_ITERATIONS $SOLVER $PROCESSES_NUM

# CPU ноды
# node41:1
# node43:1
# node44:1
# node45:1
# node46:1
# node47:1
# node48:1
# node49:1

# KNL ноды
# node32:1
# node33:1
# node34:1
# node35:1
# node36:1
# node38:1

#########################################################
# Xeon machinefile name
# XEON_MF="xeon_mf"
# # Xeon executable filename
# XEON_EXEC="stress_solver_xeon"
# # Xeon processes
# XEON_PROCESSES="2"
# # Xeon side size
# XEON_SIDE="50"

# # KNL machinefile name
# KNL_MF="knl_mf"
# # KNL executable filename
# KNL_EXEC="stress_solver_knl"
# # KNL processes
# KNL_PROCESSES="2"
# # KNL side size
# KNL_SIDE="50"

# mpiexec -f $KNL_MF -n $KNL_PROCESSES $KNL_EXEC $KNL_SIDE $SUB_ITERATIONS $SOLVER $KNL_PROCESSES

# mpiexec -f $XEON_MF -n $XEON_PROCESSES $XEON_EXEC $XEON_SIDE $SUB_ITERATIONS $SOLVER $XEON_PROCESSES