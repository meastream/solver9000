#!/bin/bash
#
# Submit as follows:
#SBATCH --job-name="Stress Solver"
#SBATCH --gres=gpu:1              # Number of GPUs per node
#SBATCH --partition=gold-batch
#SBATCH --nodes=1                 # Number of nodes
#SBATCH --ntasks-per-node=32
#SBATCH --cpus-per-task=1

# ����� srun ��������� � impi
export I_MPI_PMI_LIBRARY=/usr/lib64/libpmi.so
export LD_LIBRARY_PATH=/opt/intel2018/lib/intel64/libiomp5.so

export LD_LIBRARY_PATH=/usr/local/cuda-10.0/lib64/libOpenCL.so

source /opt/intel2017/parallel_studio_xe_2017/psxevars.sh
source /opt/intel2017/compilers_and_libraries/linux/bin/compilervars.sh intel64 -platform linux
source /opt/intel2017/impi/2017.3.196/bin64/mpivars.sh

srun --gres=gpu:1 --exclusive 0_200 0 200 1
srun 0_200 6 200 1
