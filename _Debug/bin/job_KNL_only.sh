#!/bin/bash
# Название расчитываемой задачи. Может быть любым.
#SBATCH --job-name="stress_solver KNL only"
#
# Множество вычислительных узлов для расчета задачи. Определяет характеристику
# вычислительных узлов.
#SBATCH --partition=knl-batch
# 
# Запускать расчет на нескольких узлах.
#
# Запускать каждый расчет на одном узле.
#SBATCH --nodes=2
#
# Расчетное время, после истечения которого задача будет принудительно
# остановлена. В данном случае --- 7 дней.
#SBATCH --time=7-00:00:00
#
# Количество процессов одного узла
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=64
#
# Количество потоков создаваемое каждым процессом
# Количество потоков одного процессора (20 для intelv3-batch, 24 для
# intelv4-batch, 256 для knl-batch).

# Чтобы srun заработал с impi
export I_MPI_PMI_LIBRARY=/usr/lib64/libpmi.so
export LD_LIBRARY_PATH=/opt/cuda/cuda-9.0/lib64/libOpenCL.so
export LD_LIBRARY_PATH=/opt/cuda/cuda-9.0/lib64/libOpenCL.so.1
export LD_LIBRARY_PATH=/opt/intel2018/lib/intel64/libiomp5.so
# export OMP_NUM_THREADS=10

srun stress_solver_launcher_KNL 50  1 4 8
# srun stress_solver_launcher_KNL 100 1 4 7
# srun stress_solver_launcher_KNL 300 1 4 7

# srun stress_solver_launcher_KNL 50  100 4 2
# srun stress_solver_launcher_KNL 100 100 4 2
# srun stress_solver_launcher_KNL 300 100 4 6
