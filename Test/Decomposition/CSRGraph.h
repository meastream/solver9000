#ifndef CSR_GRAPH_H

#define CSR_GRAPH_H

#include "../../FormatProviders/GridProvider/Vertex.h"
#include "../../FormatProviders/GridProvider/RLCControlReader.h"
#include "../../FormatProviders/GridProvider/RLCControlWriter.h"
#include "../../FormatProviders/GridProvider/MeshDataGraphFormatter.h"
#include "../../FormatProviders/GridProvider/ArrayVertexToGridFormatter.h"

typedef unsigned char uint8_t;

struct CSRGraph
{
	vector<int> offsets;
	vector<int> links;
	int CountVertices() const
	{
		return offsets.size()-1;
	}
};

class Graph
{
	vector<Vertex*> _points;
	vector<BoundaryCondition*> _bcs;
public:
	Graph(){ };
	Graph(const vector<Vertex*>& points, vector<BoundaryCondition*> bcs):
		_points(points), 
		_bcs(bcs) 
		{	}
	
	int CountLinks() const
	{
		int nLinks = 0;
		for(vector<Vertex*>::const_iterator it = _points.begin(); it != _points.end(); ++it)
			nLinks+=(*it)->adjVert.size();
		return nLinks;
	}

	int CountNodes() const
	{
		return _points.size();
	}

	Graph* CreateSubgraph(vector<uint8_t> const &mask, vector<int> const &fullToSub) const;

	void Load(const char* rlcFileName);	
	void Save(const char* rlcFileName);

	CSRGraph GetCSRGraph() const;
	void GetAdaptedLinks(int*& links);
	void GetAdaptedNodes(double*& nodes);
	void SetCSRGraph(const CSRGraph& csrGraph);

	void Clear();
	~Graph();

};


#endif // CSR_GRAPH_H