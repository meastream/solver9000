#include "CSRGraphDecomposer.h"

#include <metis.h>
#include "../../FormatProviders/ProviderSync/GetOffsets.h"

#include <cmath>
#include <set>


using std::pair;
using std::set;
using std::endl;


CSRGraphDecomposer::CSRGraphDecomposer()
	:
		_method(),
		_partsNumber()
{
}

void CSRGraphDecomposer::Decompose()
{
	int nVertices = _graph.CountVertices();
	real_t* rtpercents=NULL;

	if(_percents.size()>0)
	{
		rtpercents = new real_t[_partsNumber];
		double summ = 0;
		for(int i = 0; i < _percents.size(); i++)
		{
			rtpercents[i] = (real_t)_percents[i];
			summ += _percents[i];
			if(!(summ < 1.))
				printf("Exception!!! Invalid parts portions specification in decomposition (sum must be < 1)");
		}
		for(int i = _percents.size(); i < _partsNumber; i++)
		{
			rtpercents[i]=(real_t)((1.-summ)/(_partsNumber-_percents.size()));
		}	
	}

	idx_t options[METIS_NOPTIONS];
	idx_t nvtxs = (idx_t)nVertices;
	idx_t ncon = 1; 
	idx_t *xadj = &(_graph.offsets[0]);
	idx_t *adjncy = &(_graph.links[0]); 
	idx_t *vwgt = NULL;
	idx_t *vsize = NULL;
	idx_t *adjwgt = NULL;
	idx_t nparts = (idx_t)_partsNumber;
	//real_t* ubvec = NULL;
	real_t ubvec[1];
	ubvec[0]=(real_t)1.01;

	// for (int i = 0; i < _graph.offsets.size(); i++) 
	// {
	// 	std::cout << "_graph.offsets[" << i << "] = " << _graph.offsets[i] << std::endl;
	// }

	
	// for (int i = 0; i < _graph.links.size(); i++) 
	// {
	// 	std::cout << "_graph.links[" << i << "] = " << _graph.links[i] << std::endl;
	// }

	// idx_t xadj[] = { 0, 2, 5, 8, 11, 13, 16, 20, 24, 28, 31, 33, 36, 39, 42, 44 };
	// idx_t adjncy[] = { 1, 5, 0, 2, 6, 1, 3, 7, 2, 4, 8, 3, 9, 0, 6, 10, 1, 5, 7, 11, 2, 6, 8, 12, 3, 7, 9, 13, 4, 8, 14, 5, 11, 6, 10, 12, 7, 11, 13, 8, 12, 14, 9, 13 };
	//nvtxs = 15;

	//idx_t *edgecut = NULL; 
	idx_t edgecut=0;
	idx_t* part = new idx_t[nvtxs];

	METIS_SetDefaultOptions(options);

	// Метод разбивки
	options[METIS_OPTION_PTYPE]=_method;
	// options[METIS_OPTION_DBGLVL]=METIS_DBG_INFO;

	options[METIS_OPTION_NUMBERING]=0;//c-style

	// Программа должна пытаться создавать смежные разделы
	options[METIS_OPTION_CONTIG]=1;

	// Определить, что разбиение на разделы должно быть определено максимально допустимой степенью 
	// графического графа, то есть графа, в котором каждый раздел является узлом,
	// а ребра соединяют субдомены с общим интерфейсом
	options[METIS_OPTION_MINCONN]=1;

	// Определяет количество итераций для алгоритмов уточнения на каждой стадии процесса неослабления. По 
	// умолчанию 10.
	options[METIS_OPTION_NITER]=1000;

	// METIS CTYPE SHEM Sorted heavy-edge matching
	options[METIS_OPTION_CTYPE]=METIS_CTYPE_SHEM;

	// Определяет алгоритм, используемый для уточнения
	// METIS RTYPE GREEDY 
	// METIS RTYPE SEP2SIDED 
	// METIS RTYPE SEP1SIDED 
	options[METIS_OPTION_RTYPE]=METIS_RTYPE_FM;


	options[METIS_OPTION_IPTYPE]=METIS_IPTYPE_EDGE;

	// TODO: 
	// METIS_OPTION_COMPRESS 
	// options[METIS_OPTION_COMPRESS] = 1;
	// Определите, что график должен быть сжат путем объединения всех вершин в отдельные списки соответствия.

	// 

	int result = 0;
	if(_method == METIS_PTYPE_KWAY)
	{
		result = METIS_PartGraphKway(&nvtxs, &ncon, xadj, adjncy, vwgt, vsize, 
			adjwgt, &nparts, rtpercents, ubvec, options, &edgecut, part); 
	}
	else if (_method == METIS_PTYPE_RB)
	{
		result = METIS_PartGraphRecursive(&nvtxs, &ncon, xadj, adjncy, vwgt, vsize, 
			adjwgt, &nparts, rtpercents, ubvec, options, &edgecut, part); 
	}

	if(result != METIS_OK)
	{
		std::cout << "Metis result " << result;
		printf("Exception!!! Cannot perform partition");
	}

	_part.resize(nVertices);
	for(int i = 0; i < nVertices; i++)
	{
		_part[i] = part[i];
	}

	delete [] part;
	if(rtpercents != NULL)
		delete [] rtpercents;


	int redundancy;
	double maximbalance;
	double avgcount;

	BuildDecompositionData(redundancy, maximbalance, avgcount);

	std::cout << "EDGE_CUT=" << edgecut << std::endl
			<< "NODES=" << nvtxs << std::endl
			<< "REDUNDANT NODES=" << redundancy << std::endl
			<< "REDUNDANCY=" << (double)redundancy/nvtxs << std::endl
			<< "ABSOLUTE IMBALANCE=" << maximbalance << std::endl
			<< "IMBALANCE=" << (double)maximbalance/avgcount << std::endl;
}

bool CSRGraphDecomposer::IsNodeInPart(int node, int part) const
{
	return _part[node] == part;
}

// таблица синхронизации на уровне индексов вершин
int CSRGraphDecomposer::BuildOverlaps()
{		
	std::cout << "Parts numbers is: " << _partsNumber << endl;
	_overlaps.resize(_partsNumber);
	for(int i = 0; i < _partsNumber; i++)
	{
		_overlaps[i].resize(_partsNumber);
	}
	int countRedundancy = 0;

	std::cout << "_grapth.CountVertices: " << _graph.CountVertices() << endl;

	for(int i = 0; i < _graph.CountVertices(); i++)
	{
		for(int j = _graph.offsets[i]; j < _graph.offsets[i+1]; j++)
		{
			int partId=-1, adjPartId=-1;
			int adjNode = _graph.links[j];
			for(int k = 0; k < _partsNumber; k++)
			{
				if(IsNodeInPart(i, k)) partId=k;
				if(IsNodeInPart(adjNode, k)) adjPartId=k;
			}

			if(partId != adjPartId)
			{
				set<int>& cell = _overlaps[partId][adjPartId];
				if(cell.find(adjNode) == cell.end())
				{
					countRedundancy++;
					cell.insert(adjNode);
				}
			}
		}
	}
	return countRedundancy;
}

int CSRGraphDecomposer::CountOverlaps() const
{
	int nonzeroOverlaps = 0;
	for(int i = 0; i < _partsNumber; i++)
	{
		for(int j = 0; j < _partsNumber; j++)
		{
			if(_overlaps[i][j].size() > 0)
				nonzeroOverlaps++;
		}
	}
	return nonzeroOverlaps;
}
/* Выводит информации о разбиении модели:
количество частей
номер и размер подмодели, ее элементы ...
индексы перекрывающихся областей, их элементы ...
маску первоначальной модели после разбиения (0,1,...)
исходная модель с локальными индексами (с учетом перекрытий)
*/
void CSRGraphDecomposer::OutputOld(ofstream& ofs) const
{
	ofs << std::endl << _partsNumber;
	vector<int> fullToSubNoOverlaps(_part.size());
	vector<uint8_t> mask;
	for(int i = 0; i < _partsNumber; i++)
	{
		ofs << endl << i << ' ';
		ofs << _subToFullNoOverlaps[i].size();
		for(int j = 0; j < _subToFullNoOverlaps[i].size(); j++)
		{
			ofs << ' ' << _subToFullNoOverlaps[i][j];
		}

		BuildPartWithOverlapMask(i, mask);
		int id = 0;
		for(int j = 0; j < fullToSubNoOverlaps.size(); j++)
		{
			if(mask[j] > 0)
			{
				if(mask[j] == 1)
					fullToSubNoOverlaps[j]=id;
				id++;				
			}
		}
	}

	ofs << endl << endl << CountOverlaps();
	for(int i = 0; i < _partsNumber; i++)
	{
		for(int j = 0; j < _partsNumber; j++)
		{
			if(_overlaps[i][j].size() > 0)
			{
				ofs << endl << i << ' ' << j << ' ' << _overlaps[i][j].size();
				const set<int>& cell = _overlaps[i][j];
				for(set<int>::const_iterator it = cell.begin(); it != cell.end(); ++it)
					ofs << ' ' << *it + 1;
			}
		}
	}
	ofs << endl << endl;
	for(int i = 0; i < _part.size(); i++)
		ofs << _part[i] << ' ';
	ofs << endl;
	for(int i = 0; i < fullToSubNoOverlaps.size(); i++)
	{
		//ofs << "fts" << i << ":\n";
		//for(int j = 0; j < _fullToSub[i].size(); j++)
		{
			ofs << fullToSubNoOverlaps[i] << ' ';
		}
		//ofs << endl;
	}

	ofs << endl;
}

void CSRGraphDecomposer::Output(ofstream& ofs) const
{
	vector<pair<int, int> > blocks;
	ofs << _partsNumber << endl;
	for(int i = 0; i < _partsNumber; i++)
	{
		if(i > 0) ofs << endl << endl;
		ofs << i << ' ';
		ofs << _subToFull[i].size();
		GetOffsets(_subToFull[i], blocks);
		ofs << ' ' << blocks.size();
		for(int k = 0; k < blocks.size(); k++)
		{
			ofs << ' ' << blocks[k].first;
			if(blocks[k].second > 1)
				ofs << ':' << blocks[k].second;
		}
	}

	ofs << endl << endl << CountOverlaps() << endl;
	bool firstEndl = true;
	for(int i = 0; i < _partsNumber; i++)
	{
		for(int j = 0; j < _partsNumber; j++)
		{
			if(_overlaps[i][j].size() > 0)
			{
				vector<int> tmp;
				tmp.assign(_overlaps[i][j].begin(), _overlaps[i][j].end());
				GetOffsets(tmp, blocks);
				if(firstEndl) 
				{
					firstEndl = false;
				}
				else
				{
					ofs << endl << endl;
				}
				ofs << i << ' ' << j << ' ' 
					<< _overlaps[i][j].size() << ' '
					<< blocks.size();
				for(int k = 0; k < blocks.size(); k++)
				{
					ofs << ' ' << blocks[k].first;
					if(blocks[k].second > 1)
						ofs << ':' << blocks[k].second;
				}
			}
		}
	}
	ofs << endl;
}

void CSRGraphDecomposer::GetExchangeOffsets(int i, int j, 
						vector<pair<int, int> >& blocksSend, 
						vector<pair<int, int> >& blocksRecv, int nDof) const
{
	blocksSend.clear();
	blocksRecv.clear();
	//int accelerationsOffset = 0;//nDof*2*_graph.CountVertices();
	int accelerationsOffsetI = 0;//nDof*2*CountWithOverlap(i);
	int accelerationsOffsetJ = 0;//nDof*2*CountWithOverlap(j);
	nDof=1;
	vector<pair<int, int> > blocks;

	if(_overlaps[i][j].size() > 0)
	{
		vector<int> tmp;
		tmp.assign(_overlaps[i][j].begin(), _overlaps[i][j].end());
		GetOffsets(tmp, blocks);
		for(int k = 0; k < blocks.size(); k++)
		{
			// смещение в i для получения в часть i из части j
			int offsetRecv = nDof*(_fullToSub[i][blocks[k].first]-1)+
				accelerationsOffsetI;

			// смещение в j для отравки в часть i из части j
			int offsetSend = nDof*(_fullToSub[j][blocks[k].first]-1)+
				accelerationsOffsetJ;

			blocksSend.push_back(std::make_pair(offsetSend, blocks[k].second*nDof));
			blocksRecv.push_back(std::make_pair(offsetRecv, blocks[k].second*nDof));
		}
	}
}


void CSRGraphDecomposer::Init(const CSRGraph& graph, 
							  int partsNumber, 
							  const vector<double>& percents, 
							  int method)
{
	_graph = graph;
	_partsNumber = partsNumber;
	_percents = percents;
	_method = method;
}
void CSRGraphDecomposer::BuildPartWithOverlapMask(int partId, vector<uint8_t> &mask) const
{
	mask.resize(_graph.CountVertices());
	for (int i = 0; i < _graph.CountVertices(); i++)
		mask[i] = IsNodeInPart(i, partId) ? 1 : 0;
	for (int i = 0; i < _partsNumber; i++)
	{
		set<int>::const_iterator it = _overlaps[partId][i].begin();
		while(it != _overlaps[partId][i].end())
		{
			mask[*it++] = 2;
		}
	}
}

int CSRGraphDecomposer::CountWithOverlap(int partId) const
{
	int count = 0;
	for (int i = 0; i < _graph.CountVertices(); i++)
		if(IsNodeInPart(i, partId))
			count++;
	for (int i = 0; i < _partsNumber; i++)
			count+=_overlaps[partId][i].size();
	return count;
}
/** Построение отображений индексов на номеры полной модели и частей
* отображение делается с учетом перекрывающих слоёв
* индекс = номер - 1
* для fullToSub нулевой номер означает, что этого узла нет в подмодели
* _subToFull и _fullToSub  отображают индексы на номера
*/
void CSRGraphDecomposer::BuildMaps()
{
	_subToFull.resize(_partsNumber);
	_subToFullNoOverlaps.resize(_partsNumber);
	_fullToSub.resize(_partsNumber);
	int nVertices = _graph.CountVertices();
	vector<uint8_t> mask;
	for (int j = 0; j < nVertices; j++)
	{
		_subToFullNoOverlaps[_part[j]].push_back(j+1);
	}

	for (int i = 0; i < _partsNumber; i++)
	{
		BuildPartWithOverlapMask(i, mask);
		for (int j = 0; j < nVertices; j++)
			if (mask[j])
				_subToFull[i].push_back(j+1);
	}

	for (int i = 0; i < _partsNumber; i++)
	{
		_fullToSub[i].resize(nVertices);
		for (int j = 0; j < _subToFull[i].size(); j++)
			_fullToSub[i][_subToFull[i][j]-1] = j+1;
	}
}


void CSRGraphDecomposer::BuildDecompositionData(int& redundancy,
	double& maximbalance, double& avgcount)
{
	redundancy = BuildOverlaps();
	BuildMaps();

	vector<int> counts(_partsNumber, 0);

	for(int i = 0; i < _partsNumber; i++)
	{
		counts[i] = CountWithOverlap(i);
	}
	avgcount = 0;
	maximbalance = 0.;

	vector<double> rtpercents(_partsNumber);
	double summ = 0;

	for(int i = 0; i < _percents.size(); i++)
	{
		rtpercents[i] = _percents[i];
		summ += _percents[i];
	}
	for(int i = _percents.size(); i < _partsNumber; i++)
	{
		rtpercents[i]=(1.-summ)/(_partsNumber-_percents.size());
	}	

	for(int i = 0; i < _partsNumber; i++)
	{
		avgcount+=counts[i];
		double imbalance=fabs(counts[i]-rtpercents[i]*(redundancy+_graph.CountVertices()));
		if(imbalance > maximbalance)
			maximbalance = imbalance;
	}
	avgcount/=(double)_partsNumber;
}	

