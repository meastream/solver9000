#ifndef CSRGraphDecomposerH
#define CSRGraphDecomposerH

#include "CSRGraph.h"
#include "../../metis-5.1.0/include/metis.h"

#include <set>

using std::set;
using std::ofstream;
using std::pair;

class CSRGraphDecomposer
{
//  инициализируемые поля
	CSRGraph _graph;
	vector<double> _percents;
	int _method;
	int _partsNumber;
	
//	производные поля
	vector<int> _part;	// разбиение
	vector<vector<int> > _subToFull;
	vector<vector<int> > _subToFullNoOverlaps;
	vector<vector<int> > _fullToSub;
	vector<vector<set<int> > > _overlaps;

	int BuildOverlaps();
	void BuildMaps();
	int CountWithOverlap(int partId) const;
public:

	CSRGraphDecomposer();

	void Init
		(
			const CSRGraph& graph,
			int partsNumber,
			const vector<double>& percents,
			int method
		);
	
	void Decompose();

	void BuildDecompositionData(int& redundancy, double& maximbalance, double& avgcount);

	// для теста
	void SetPart(const vector<int>& part) {_part = part;};		

	bool IsNodeInPart(int node, int part) const;

	// таблица синхронизации на уровне индексов вершин
	void Output(ofstream& ofs) const;
	void OutputOld(ofstream& ofs) const;
	int CountOverlaps() const;
	
	/** Строит маску соответствия принадлежности вершин части ( с перекрытиями )
	* mask[i]=0 - вершина i не принадлежит, mask[i]=1 - принадлежит
	* @param partId - номер части
	* @param mask - маска
	*/
	void BuildPartWithOverlapMask(int partId, vector<uint8_t> &mask) const;
	
	const vector<int>& GetFullToSub(int partId) const {return _fullToSub[partId];}

	void GetExchangeOffsets(int i, int j, 
		 vector<pair<int, int> >& blocksSend, 
		 vector<pair<int, int> >& blocksRecv, int nDof) const;
};

#endif