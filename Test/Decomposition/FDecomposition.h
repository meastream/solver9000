#ifndef FDecompositionH
#define FDecompositionH
#include "CSRGraphDecomposer.h"
#include "../Wrappers/FileRoutines.h"
#include "../../FormatProviders/ProviderSync/SyncTable.h"
/** Класс для декомпозиции сеток
*/
class FDecomposition
{
	CSRGraphDecomposer _decomposer;
	Graph _graph;
	string _fileName;
	int _partsNumber;
	vector<double> _percents;
	SyncTable _table;

public:

	FDecomposition();

	FDecomposition
		(
			char const* fileName,
			int partsNumber,
			const vector<double>& percents
		);

	void Init(char const * fileName, int partsNumber, const vector<double>& percents);
	void Init(const Graph& graph, int partsNumber, const vector<double>& percents);
	int Decompose();
	void BuildSyncTable(int nDof, SyncTable& syncTable) const;
	void SetFileName(const string& fileName)
	{
		_fileName = fileName;
	}

	void Output() const;
	void OuputCompressed() const;
	void OuputGrids() const;
	void OuputSyncTable(int nDofs) const;

	// для теста
	void BuildDecompositionData(const vector<int>& part)
	{
		int redundancy;
		double maximbalance;
		double avgcount;
		_decomposer.Init(_graph.GetCSRGraph(), _partsNumber, _percents, METIS_PTYPE_KWAY);
		_decomposer.SetPart(part);
		_decomposer.BuildDecompositionData(redundancy,maximbalance,avgcount);
	}
};


#endif