#include <metis.h>
#include <iostream>

int main()
{
	//create graph
	//int nNodes=7;
	//int nConstraints=1;
	//int nParts=2;
	//int nLinks = 8;

	//int xadj[] = {0,2,4,7,10,12,14,16};				// nNodes+1
	//int adj[] = {1,5,0,2,1,3,5,2,4,6,3,6,0,2,3,4};	// nLinks*2

	int nNodes=15;
	int nConstraints=1;
	int nParts=2;
	int nLinks = 22;
	int xadj[] = {0,2,5,8,11,13,16,20,24,28,31,33,36,39,42,44};				// nNodes+1
	int adj[] = {1,5,0,2,6,1,3,7,2,4,8,3,9,0,6,10,1,5,7,11,2,6,8,12,3,7,9,13,4,8,14,5,11,6,10,12,7,11,13,8,12,14,9,3};	// nLinks*2


	int* parts = new int[nNodes];
	int* edgecut = new int[nLinks*2];

	idx_t options[METIS_NOPTIONS];
	METIS_SetDefaultOptions(options);

	//options[METIS_OPTION_PTYPE] = METIS_PTYPE_KWAY;
	//options[METIS_OPTION_OBJTYPE] = METIS_OBJTYPE_CUT;
	//options[METIS_OPTION_CTYPE] = METIS_CTYPE_RM;
	//options[METIS_OPTION_IPTYPE] = METIS_IPTYPE_GROW;
	//options[METIS_OPTION_RTYPE] = METIS_RTYPE_FM;
	//options[METIS_OPTION_PTYPE] = 0;
	//options[METIS_OPTION_OBJTYPE] = 2;
	//options[METIS_OPTION_CTYPE] = 1;
	//options[METIS_OPTION_IPTYPE] = 2;
	//options[METIS_OPTION_RTYPE] = 0;
  //METIS_OPTION_OBJTYPE,//1
  //METIS_OPTION_CTYPE,//2
  //METIS_OPTION_IPTYPE,//3
  //METIS_OPTION_RTYPE,//4
	real_t targetWeights[] = {0.2,0.8};
	//partition

	if(METIS_PartGraphRecursive(&nNodes,&nConstraints,xadj,adj,NULL,NULL,NULL,&nParts,targetWeights,NULL,options, edgecut, parts) == METIS_OK)
	{
		std::cout << "Success!\n";
		for(int i = 0; i < nNodes; i++)
		{
			std::cout << i << ":\t" << parts[i] << std::endl;
		}
	}
	delete [] parts;
	delete [] edgecut;
	return 0;
}