#include "FDecomposition.h" 
#include "../Wrappers/StringRoutines.h" 
#include "../../FormatProviders/ProviderSync/SyncTableProvider.h"
//#include "../fcore.h"
#define EXT_DECOMPOSITION	".dec"

FDecomposition::FDecomposition()
	:
		_partsNumber()
{
}

FDecomposition::FDecomposition
	(
		char const* fileName,
		int partsNumber,
		const vector<double>& percents
	)
{
	Init(fileName, partsNumber, percents);
}

void FDecomposition::Init(char const *fileName, int partsNumber, const vector<double>& percents)
{
	_fileName = fileName;
	_partsNumber = partsNumber;
	_percents = percents;
	_graph.Load(_fileName.c_str());
}

void FDecomposition::Init(const Graph& graph, int partsNumber, const vector<double>& percents)
{
	_fileName = "";
	_partsNumber = partsNumber;
	_percents = percents;
	_graph = graph;
}


void FDecomposition::Output() const
{
	string newFile = fs::SplitFileFromExt(_fileName) + EXT_DECOMPOSITION;
	ofstream ofs(newFile.c_str());
	if (ofs.is_open())
	{
		_decomposer.OutputOld(ofs);
		ofs.close();
	}
}

void FDecomposition::OuputCompressed() const
{
	string newFile = fs::SplitFileFromExt(_fileName) + "c" + EXT_DECOMPOSITION;
	ofstream ofs(newFile.c_str());
	if (ofs.is_open())
	{
		_decomposer.Output(ofs);
		ofs.close();
	}
}

void FDecomposition::OuputGrids() const
{
	vector<uint8_t> mask;
	string filePrefix = fs::SplitFileFromExt(_fileName);
	fs::DeleteFilesWithPostfix("*.rlc", filePrefix, '_', 4);
	for(int i = 0; i < _partsNumber; i++)
	{
		_decomposer.BuildPartWithOverlapMask(i, mask);
		Graph* subgraph = _graph.CreateSubgraph(mask, _decomposer.GetFullToSub(i));
		//std::cout << "Number of links: " << subgraph->CountLinks() << " of " << i << std::endl;
		string newFile = filePrefix + "_" + NumberToString(i) + ".rlc";
		subgraph->Save(newFile.c_str());
		delete subgraph;
	}
}

void FDecomposition::BuildSyncTable(int nDof, SyncTable& syncTable) const
{
	vector<pair<int, int> > sendOffsets;
	vector<pair<int, int> > recvOffsets;
	vector<pair<int, int> >::iterator it;
	
	syncTable.Allocate(_partsNumber);
	for(int i = 0; i < _partsNumber; i++)
	{
		for(int j = 0; j < _partsNumber; j++)
		{
			_decomposer.GetExchangeOffsets(i, j, sendOffsets, recvOffsets, nDof);
			for (it = sendOffsets.begin(); it != sendOffsets.end(); ++it)
			{
				syncTable[j][i].AddSendOffsets(it->first, it->second);
			}
			
			for (it = recvOffsets.begin(); it != recvOffsets.end(); ++it)
			{
				syncTable[i][j].AddRecvOffsets(it->first, it->second);
			}
		}
	}
}

void FDecomposition::OuputSyncTable(int nDofs) const
{
	SyncTable syncTable;
	BuildSyncTable(nDofs, syncTable);
	SyncTableProvider provider;
	provider.Init(syncTable, 1, 0); // TODO: заменить на 0 (как и было) или переделать
	provider.Write(fs::SplitFileFromExt(_fileName)+".sync");
}

int FDecomposition::Decompose()
{ 
	_decomposer.Init(_graph.GetCSRGraph(), _partsNumber, _percents, METIS_PTYPE_KWAY);
	_decomposer.Decompose();
	return 0;
}

