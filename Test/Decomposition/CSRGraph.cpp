#include "CSRGraph.h"

void Graph::Load(const char* rlcFileName)
{
	RLCControlReader *rlcControlReader = new RLCControlReader();
	if(!rlcControlReader->ReadMeshFromFile(rlcFileName))
		printf("Exception!!! File not found: ", rlcFileName);

	OccRectilinearGrid* rectilinearGrid = rlcControlReader->GetGrid();
	MeshDataGraphFormatter *mesh =new MeshDataGraphFormatter(rectilinearGrid);
	mesh->Vectorize();
	_points = mesh->GetArrayOfVertex();
	//_bcs = mesh->GetArrayOfBC();
	_bcs = rlcControlReader->GetBC();

	for (int i=0; i<_points.size(); i++)
	{
		_points[i]->size = _points[i]->adjVert.size();
	}		

	delete mesh;
}

CSRGraph Graph::GetCSRGraph() const
{
	CSRGraph graph;
	graph.offsets.resize(_points.size()+1);
	graph.offsets[0] = 0;
	graph.links.reserve(_points.size()*6);
	for(int i = 0; i < _points.size(); i++)
	{
		graph.offsets[i+1] = graph.offsets[i]+_points[i]->size;
		for(int j = 0; j < _points[i]->size; j++)
		{
			graph.links.push_back(_points[i]->adjVert[j] - 1);
		}
	}
	return graph;
}

void Graph::GetAdaptedLinks(int*& links)
{
	int offset = 0;
	for (int i = 0; i < _points.size(); i++)
	{
		int curNode = _points[i]->_globalNumber - 1;
		for (int j = 0; j < _points[i]->size; j++)
			if (_points[i]->adjVert[j] > i)
			{
				links[offset++] = curNode;
				//printf("\n%d ", curNode);
				links[offset++] = _points[i]->adjVert[j] - 1;
				//printf("%d ", _points[i]->adjVert[j] - 1);
			}
	}
}

void Graph::GetAdaptedNodes(double*& nodes)
{
	int offset = 0;
	for (int i = 0; i < _points.size(); i++)
	{
		nodes[offset++] = _points[i]->pt.x * 0.001;
		nodes[offset++] = _points[i]->pt.y * 0.001;
		nodes[offset++] = _points[i]->pt.z * 0.001;
		//printf("\nnode %d: %.1lf %.1lf %.1lf", i, _points[i]->pt.x* 0.001, _points[i]->pt.y* 0.001, _points[i]->pt.z* 0.001);
	}
}

void Graph::SetCSRGraph(const CSRGraph& csrGraph)
{
	Clear();
	this->_points.resize(csrGraph.CountVertices());
	for(int i = 0; i < csrGraph.CountVertices(); i++)
	{
		Vertex* vertex = new Vertex();
		for(int j = csrGraph.offsets[i]; j < csrGraph.offsets[i+1]; j++)
		{
			vertex->adjVert.push_back(csrGraph.links[j]);
		}
		vertex->weight = 0.;
		vertex->size = vertex->adjVert.size();
		vertex->_localNumber = vertex->_globalNumber = i;

		_points[i] = vertex;
	}
}

void Graph::Clear()
{
	vector<Vertex* >::iterator it = _points.begin();
	while(it != _points.end())
		delete *it++;
	
	vector<BoundaryCondition* >::iterator itBc = _bcs.begin();
	while(itBc != _bcs.end())
		delete *itBc++;
}

Graph::~Graph()
{
	Clear();
}

Graph* Graph::CreateSubgraph(vector<uint8_t> const &mask, vector<int> const &fullToSub) const
{
	// check
	if ((fullToSub.size() != mask.size()) || (fullToSub.size() != _points.size() || (_points.size() != mask.size())))
	{
		printf("Exception!!! Impossible to create subgraph");
	}
	vector<int> adjVertexes;
	adjVertexes.reserve(6);
	vector<Vertex*> subgraph;
	for (int i = 0; i < _points.size(); i++)
	{
		int vertexId = _points[i]->_globalNumber-1;
		if (mask[vertexId])
		{
			Vertex *newVertex = new Vertex(_points[i]);
			newVertex->_globalNumber = fullToSub[vertexId]; // from 1
			adjVertexes.clear();
			for (int j = 0; j < newVertex->adjVert.size(); j++)
			{
				int adjVertexId = newVertex->adjVert[j]-1;
				if (mask[adjVertexId])
				{
					adjVertexes.push_back(fullToSub[adjVertexId]);
				}
			}
			newVertex->adjVert = adjVertexes;
			subgraph.push_back(newVertex);
		}
	}
	
	vector<BoundaryCondition*> bcs;
	vector<int> bcIds;
	for (int i = 0; i < _bcs.size(); i++)
	{
		bcIds.clear();
		bcIds.reserve(_bcs[i]->CountPoints());
		for(int j = 0; j < _bcs[i]->CountPoints(); j++)
		{
			int tmpId = (*_bcs[i])[j] - 1;
			if(mask[tmpId])
			{
				bcIds.push_back(fullToSub[tmpId]);
			}
		}
		if(bcIds.size() > 0)
		{
			BoundaryCondition* bc = new BoundaryCondition(
				_bcs[i]->GetName(),
				_bcs[i]->GetVar(),
				_bcs[i]->GetID(),
				bcIds);
			bcs.push_back(bc);
		}

	}
	
	return new Graph(subgraph, bcs);
}

void Graph::Save(char const*rlcFileName)
{
	// TODO: Boundaries
	ArrayVertexToGridFormatter formatter(_points);
	if (formatter.Transform())
	{
		RLCControlWriter writer(formatter.GetGrid(), _bcs);
		if (!writer.DumpMeshToFile(rlcFileName))
		{
			printf("Exception!!! writer.DumpMeshToFile(rlcFileName)");
		}
	}
	else
	{
		printf("Exception!!! fomatter.Transform()");
	}
}


