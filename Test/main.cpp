#include <iostream>
#include "ComparativeTest.h"
#include "StressSolverTest.h"
#include "TestFactory.h"
#include <sstream>
#include "mpi.h"
#include <metis.h>
#include "../Solvers/Stress/FTimer.h"

// #define MPI_DECOMPOSITION
// #define USE_AVX

// mpirun -n 2 -hosts node44.cluster,node43.cluster ./stress_solver_launcher 50 1

/* Специальные флаги компиляции:
MPI_DECOMPOSITION	Расчет с декомпозицией (Test, stress)
OMP_SOLVE			Расчет с OpenMP (stress, Test)
USA_AVX				Расчет с AVX (stress)
USE_KNC				Расчет на MIC (stress)
USE_KNL				Расчет на KNL (stress) 
USE_SVML			Расчет с кэшированием тригонометрии (stress) - совместно с KNC, KNL или AVX 
SET_FORCE			Включает задание сил и граничных условий - для MPI не работает !!! (Test)
*/

int main(int argc, char *argv[])
{
	using namespace SpecialSolversTest;
	using namespace SpecialSolversTest::StressStrainStuff;

	// Установка параметров расчета
	std::stringstream stringstr;

	/* Типы решателей:
	0 - без векторизации
	1 - AVX
	2 - AVX+FMA
	3 - KNC/KNL
	4 - KNC/KNL с полным использованием регистров
	5 - SSE
	6,7 - без выравнивания (лучше не надо!)
	*/

	int solverType = 2;
	int numberOfElements = 100;//40; // длина стороны пластины
	int numberOfSubiterations = 100; 
	int submodelsCount = 2;
	int timeStamp = 0;

	if (argc == 2)
	{
		stringstr << argv[1];
		stringstr >> numberOfElements;
	}
	else
		if (argc == 3)
		{
			stringstr << argv[1] << ' ' << argv[2];
			stringstr >> numberOfElements >> numberOfSubiterations;
		}
		else
		{
			if (argc == 4)
			{
				stringstr << argv[1] << ' ' << argv[2] << ' ' << argv[3];
				stringstr >> numberOfElements >> numberOfSubiterations >> solverType;
			}
			if (argc == 5)
			{
				stringstr << argv[1] << ' ' << argv[2] << ' ' << argv[3] << ' ' << argv[4];
				stringstr >> numberOfElements >> numberOfSubiterations >> solverType >> submodelsCount;
			}
			if (argc == 6)
			{
				stringstr << argv[1] << ' ' << argv[2] << ' ' << argv[3] << ' ' << argv[4];
				stringstr >> numberOfElements >> numberOfSubiterations >> solverType >> submodelsCount >> timeStamp;
			}
		}

	std::cout << "------------------------------" << std::endl
		<< "    STRESS TEST" << std::endl
		<< "------------------------------" << std::endl;
#ifdef INTEL_AVX
	std::cout << "INTEL AVX\n";
#endif
	std::cout << "Elements:" << numberOfElements << 'x' << numberOfElements
		<< "\nIterations:" << numberOfSubiterations << 'x' << 100
		<< "\nSolver Type:" << solverType << std::endl;

	// Запуск инициализации и расчета
#ifdef MPI_DECOMPOSITION
	std::time_t result = std::time(nullptr);

	std::stringstream ss;
	ss << result;
	std::string ts = ss.str();

	ss.flush();

	string filename = ts + "-DecompositionTest";

	// string filename = "DecompositionTest";

	// Инициализация MPI-среды
	MPI_Init(&argc, &argv);
	int numberOfProcs, procNumber;
	MPI_Comm_size(MPI_COMM_WORLD, &numberOfProcs);
	MPI_Comm_rank(MPI_COMM_WORLD, &procNumber);

	std::cout << "---------- MPI SOLVE ----------" << std::endl;
	std::cout << "Processes:" << numberOfProcs << std::endl;
	


	// int submodelsCount = 100;

	if (procNumber == 0)
	{
		// Генерация тестовой модели
		MakeQuaterPlate(0.8, numberOfElements, filename);
		std::cout << "------------------------------" << std::endl
			<< "    MODEL CREATED" << std::endl;

		// Декомпозиция тестовой модели

		/*	Если части не равные - задаем массив процентных соотношений
			Если равные - подаем пустой массив (по умолчанию поровну) */
		std::vector<double> percents; // соотношение частей подмоделей (без последней части ??)
		// int submodelsCount = 100;

		percents.push_back(1.0/submodelsCount);
		// percents.push_back(0.90);
		
		std::cout << "------------------------------" << std::endl
			<< "    MODEL DECOMPOSITION DATA:" << std::endl;

		DecomposeTestGridFromFile(filename + ".rlc", submodelsCount, percents);
		std::cout << "    DECOMPOSITION COMPLETED" << std::endl
			<< "------------------------------" << std::endl;
	}

	// Ожидание инициализации
	MPI_Barrier(MPI_COMM_WORLD);

	// Расчет с декомпозицией
	// int solverTypes [40] { 
	// 	2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
	// 	2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
	// 	2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
	// 	2, 2, 2, 2, 2, 2, 2, 2, 2, 2
	// 	// 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	// 	// 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	// 	// 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	// 	// 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	// 	// 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
	// 	// 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
	// 6 2 2
	// };

	int *solverTypes = new int[submodelsCount];
	int gpuNodesCount = 0;
	int knlNodesCount = 6;

	for (int i = 0; i < submodelsCount; i++) 
	{
		if (gpuNodesCount > 0 && i <= gpuNodesCount - 1) 
		{
			solverTypes[i] = 6;	
		}
		// if (knlNodesCount > 0 && i <= knlNodesCount - 1) 
		// {
		// 	solverTypes[i] = 4;	
		// }
		else
		{
			solverTypes[i] = solverType;
		}		
	}

	// PerformanceOmpCounter pcMpiSolve;
	// pcMpiSolve.Start();
	Test1xXxXaMPI(solverTypes[procNumber], 0.8, 0.01, numberOfElements, numberOfSubiterations, filename);
	// pcMpiSolve.Stop();

	// char pcMpiSolveText[128];
	// sprintf(pcMpiSolveText, "MPI solver Total: ");
	// pcMpiSolve.Print(pcMpiSolveText, false);



	MPI_Finalize();

	delete [] solverTypes;
#else
	std::cout << "-------- NO MPI SOLVE --------" << std::endl;
	Test1xXxXa(solverType, 0.8, 0.01, numberOfElements, numberOfSubiterations);
#endif

	//getchar();
	return 0;
}

// RHS calculation based on random data test
//std::cout << (ComparativeTest() ? "PASSED!\n" : "NOT PASSED\n");

//Test1x1x3(0, xlr);
//Test1x51x51(1, xlr);
//Test1x11x11(1, xlr);
//Test1x22x22a(0, xlr);
//Test1x44x44a(0, xlr);
//Test1x100x100a(0, xlr);
//Test10x3x3(1, xlr);
//Test50x5x5(1, xlr);
//Test10x5x5(1, xlr);
//Test10x7x7(1, xlr);
//Test1xXxXa(0, 0.8, 0.01, 10);
//Test1xXxXa(0, 0.8, 0.01, 20);
//Test1xXxXa(0, 0.8, 0.01, 30);
//Test1xXxXa(0, 0.8, 0.01, 100);
//Test1xXxXa(0, 0.8, 0.01, 80);
//Test1xXxXa(0, 0.8, 0.01, 1000);
//Test1xXxXa(1, 0.8, 0.01, 50);

/*if (argc == 2)
{
stringstr << argv[1];
stringstr >> numberOfElements;
}
else
if (argc == 3)
{
stringstr << argv[1] << ' ' << argv[2];
stringstr >> numberOfElements >> numberOfSubiterations;
}
else
if (argc == 4)
{
stringstr << argv[1] << ' ' << argv[2] << ' ' << argv[3];
stringstr >> numberOfElements >> numberOfSubiterations >> solverType;
}*/