#ifndef FC_UNISTD_H

#define FC_UNISTD_H


#ifdef GNUCPP

#include "StringRoutines.h"

#include <unistd.h>
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <list>
#include <map>
#include <stack>
#include <cstring>

using std::string;
using std::fstream;
using std::iostream;
using std::vector;
using std::list;
using std::map;
using std::stack;

#endif


#endif // FC_UNISTD_H