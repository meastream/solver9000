#pragma once


#include "CommonSolvers.h"

#include <string>
#include <vector>
using namespace std;

namespace SpecialSolversTest
{

	using namespace SpecialSolvers;


	// ��������������� ������� �������� �����
	
	// ������� �������� ����� nx x ny x nz � ����� step
	// ���������� ����� = nx x ny x nz
	// ���������� ������ nLinks
	void CreateTestGrid
		(
			double*& nodes,
			int*& links,
			int& nLinks,
			int nx,
			int ny,
			int nz,
			double step,
			const std::string& fileName
		);

	void CreateTestGrid
		(
			const GridParams& gridParams,
			const std::string& rlcGridFileName
		);

	// ���������� �����, ���������� � RLC-����
	void ReadTestGridFromFile
		(
		double*& nodes,
		int& nNodes,
		int*& links,
		int& nLinks,
		//int nx,
		//int ny,
		//int nz,
		//double step,
		const string& fileName
		);

	void DecomposeTestGridFromFile
		(
		string const rlcGridFileName,
		int partsNumber,
		vector<double>& percents
		);
}