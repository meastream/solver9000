#include <CL/cl.h>
#include "StressStrainCppIterativeSolverOpenCL.h"
#include "StressStrainCppSolver.h"
#include "../../AdditionalModules/fmath/Vector3.h"
#include "Common.h"
#include "../../AdditionalModules/fmath/Matrix3x3.h"
#include "../../AdditionalModules/fmath/Matrix3x4.h"
#include "mpi.h"
#include "../../FormatProviders/ProviderSync/SyncTable.h"
#include "../../FormatProviders/ProviderSync/SyncTableProvider.h"
#include "../../FormatProviders/ProviderSync/MPISynchronisation.h"
#include <stdlib.h>

#define MAX_SOURCE_SIZE 500000

//#define USE_EXP \\ set this flag when targeting experimental platforms. Otherwise comment it out
//#define AMD

#define PLATFROM_NAME "NVIDIA"

using namespace MathHelpers;
namespace Stress
{

	Stress::StressStrainCppIterativeSolverOpenCL::StressStrainCppIterativeSolverOpenCL
	(
		double * params,
		int * links,
		int nLinks,
		double * coordinates,
		int nElements,
		double gridStep,
		double timeStep,
		int numThreads,
		int stride,
		string syncTableFileName
	) :
		StressStrainCppIterativeSolver
		(
			params,
			links,
			nLinks,
			coordinates,
			nElements,
			gridStep,
			timeStep,
			numThreads,
			stride,
			syncTableFileName
		)
	{
		SetUid("OpenCL_Solver");
		std::cout << "OpenCL SOLVER" << std::endl << std::flush;
		allBoundaryForces = 0;
		FreeDegrees = 0;
		this->syncTableFileName = syncTableFileName;
	}

	Stress::StressStrainCppIterativeSolverOpenCL::~StressStrainCppIterativeSolverOpenCL()
	{

	}

	void Stress::StressStrainCppIterativeSolverOpenCL::InitialSolve()
	{
		_rotationSolver->InitialSolve();
		InitOpenCLContext();
		clInitKernel();
		InitBuffers();
		StressStrainCppIterativeSolver::CalculateForces();
		UploadAllOpenCLBuffers();
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::InitOpenCLContext()
	{
		cl_int code = 0;
		cl_platform_id platformId;
		cl_uint numPlatforms = 0;
		cl_uint numDevices = 0;


		cl_platform_id * platforms = NULL;
		char vendor_name[128] = { 0 };
		cl_uint num_platforms = 0;

		// get number of available platforms
		cl_int err = clGetPlatformIDs(0, NULL, &num_platforms);
		if (CL_SUCCESS != err)
		{
			exit(err);
		}
		platforms = (cl_platform_id*)malloc(sizeof(platformId)* num_platforms);
		if (NULL == platforms)
		{
			// handle error
		}
		err = clGetPlatformIDs(num_platforms, platforms, NULL);
		if (CL_SUCCESS != err)
		{
			// handle error
		}
		// return the OpenCL 2.1 development environment platform
		for (cl_uint ui = 0; ui < num_platforms; ++ui)
		{
			err = clGetPlatformInfo(platforms[ui],
				CL_PLATFORM_NAME,
				128 * sizeof(char),
				vendor_name,
				NULL);
			if (CL_SUCCESS != err)
			{
				// handle error
			}
			if (vendor_name != NULL)
			{
				if (strstr(vendor_name, PLATFROM_NAME))
				{
					platformId = platforms[ui];
					break;
				}
			}
		}
		code = clGetDeviceIDs(platformId, CL_DEVICE_TYPE_GPU, 1, &deviceId, &numDevices);

		puts(vendor_name);
		if (code != 0)
		{
			fprintf(stderr, "Failed to get device ids. %d\n", code);
			exit(code);
		}
		clContext = clCreateContext(NULL, 1, &deviceId, NULL, NULL, &code);
		if (code != 0)
		{
			fprintf(stderr, "Failed to create cotext. %d\n", code);
			exit(code);
		}
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::InitBuffers()
	{

#ifdef MPI_DECOMPOSITION
		int rank, size;
		MPI_Comm_size(MPI_COMM_WORLD, &size);
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);

		// ������������� ������� �������������
		pcSync.Start();
		_syncTableGpu = SyncTable(size);
		SyncTableProvider syncProvider = SyncTableProvider();
		GpuSyncTableProvider gpuTableProvider = GpuSyncTableProvider(size);
		syncProvider.Read(syncTableFileName + ".sync");
		_syncTableGpu = syncProvider.GetTable();
		_gpuSyncTableProvider = gpuTableProvider;
		pcSync.Stop();
#endif

		int ret = 0;
		DataInternalBuffer = clCreateBuffer(clContext, CL_MEM_READ_WRITE, 6 * sizeof(cl_double)*vecStride*_nElements, NULL, &ret);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to create buffers.\n");
			exit(ret);
		}
		DataRotationMatrixBuffer = clCreateBuffer(clContext, CL_MEM_READ_WRITE, matStride * sizeof(cl_double)*_nElements, NULL, &ret);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to create buffers.\n");
			exit(ret);
		}

		LinkedElemenetsBuffer = clCreateBuffer(clContext, CL_MEM_READ_WRITE, sizeof(cl_int)*_nElements * 6, NULL, &ret);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to create buffers.\n");
			exit(ret);
		}
		RadiusVectorsBuffer = clCreateBuffer(clContext, CL_MEM_READ_WRITE, vecStride * 6 * sizeof(cl_double), NULL, &ret);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to create buffers.\n");
			exit(ret);
		}
		BoundaryForcesBuffer = clCreateBuffer(clContext, CL_MEM_READ_WRITE, vecStride2 * sizeof(cl_double) * _nElements, NULL, &ret);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to create buffers.\n");
			exit(ret);
		}
		BoundaryFixedBuffer = clCreateBuffer(clContext, CL_MEM_READ_WRITE, 6 * sizeof(cl_int)*_nElements, NULL, &ret);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to create buffers.\n");
			exit(ret);
		}
		ElementStressFactorCacheBuffer = clCreateBuffer(clContext, CL_MEM_READ_WRITE, vecStride * sizeof(cl_double)*_nElements, NULL, &ret);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to create buffers.\n");
			exit(ret);
		}
		StressScalingFactorBuffer = clCreateBuffer(clContext, CL_MEM_READ_WRITE, vecStride * sizeof(cl_double)*_nElements, NULL, &ret);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to create buffers.\n");
			exit(ret);
		}
		StressBuffer = clCreateBuffer(clContext, CL_MEM_READ_WRITE, 2 * vecStride * sizeof(cl_double)*_nElements, NULL, &ret);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to create buffers.\n");
			exit(ret);
		}
		CoordinatesBuffer = clCreateBuffer(clContext, CL_MEM_READ_WRITE, 3 * sizeof(cl_double)*_nElements, NULL, &ret);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to create buffers.\n");
			exit(ret);
		}
	}
	void Stress::StressStrainCppIterativeSolverOpenCL::UploadPartialOpenCLBuffers()
	{
		int ret = 0;

		DataInternal = (cl_double*)_dataInternal;
		ret = clEnqueueWriteBuffer(command_queue, DataInternalBuffer, CL_FALSE, 0, 6 * sizeof(cl_double) * vecStride * _nElements, DataInternal, 0, NULL, NULL);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to write buffers.\n");
			exit(ret);
		}
		ret = clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&DataInternalBuffer);
		ret = clSetKernelArg(kernelRK1, 0, sizeof(cl_mem), (void *)&DataInternalBuffer);
		ret = clSetKernelArg(kernelRK2, 0, sizeof(cl_mem), (void *)&DataInternalBuffer);
		ret = clSetKernelArg(kernelRK3, 0, sizeof(cl_mem), (void *)&DataInternalBuffer);
		ret = clSetKernelArg(kernelRK4, 0, sizeof(cl_mem), (void *)&DataInternalBuffer);
		if (ret != 0)
		{
			fprintf(stderr, "Failed with args setup.\n");
			exit(ret);
		}
		DataRotationMatrix = (cl_double*)_dataRotationMtx;
		if (ret != 0)
		{
			fprintf(stderr, "Failed to create buffers.\n");
			exit(ret);
		}
		ret = clEnqueueWriteBuffer(command_queue, DataRotationMatrixBuffer, CL_FALSE, 0, matStride * sizeof(cl_double)*_nElements, DataRotationMatrix, 0, NULL, NULL);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to write buffers.\n");
			exit(ret);
		}
		ret = clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&DataRotationMatrixBuffer);
		ret = clSetKernelArg(kernelRK1, 1, sizeof(cl_mem), (void *)&DataRotationMatrixBuffer);
		ret = clSetKernelArg(kernelRK2, 1, sizeof(cl_mem), (void *)&DataRotationMatrixBuffer);
		ret = clSetKernelArg(kernelRK3, 1, sizeof(cl_mem), (void *)&DataRotationMatrixBuffer);
		ret = clSetKernelArg(kernelRK4, 1, sizeof(cl_mem), (void *)&DataRotationMatrixBuffer);
		if (ret != 0)
		{
			fprintf(stderr, "Failed with args setup.\n");
			exit(ret);
		}
		Stress = _stress;
		ret = clEnqueueWriteBuffer(command_queue, StressBuffer, CL_TRUE, 0, 2 * vecStride * sizeof(cl_double)*_nElements, Stress, 0, NULL, NULL);
		ret = clSetKernelArg(kernel, 8, sizeof(cl_mem), (void *)&StressBuffer);
		ret = clSetKernelArg(kernelRK1, 8, sizeof(cl_mem), (void *)&StressBuffer);
		ret = clSetKernelArg(kernelRK2, 8, sizeof(cl_mem), (void *)&StressBuffer);
		ret = clSetKernelArg(kernelRK3, 8, sizeof(cl_mem), (void *)&StressBuffer);
		ret = clSetKernelArg(kernelRK4, 8, sizeof(cl_mem), (void *)&StressBuffer);

	}

	void Stress::StressStrainCppIterativeSolverOpenCL::UploadAllOpenCLBuffers()
	{
		//� ������
		UploadPartialOpenCLBuffers();

		int ret = 0;


		cl_uint* LinkedElements = (cl_uint*)_linkedElements;
		ret = clEnqueueWriteBuffer(command_queue, LinkedElemenetsBuffer, CL_FALSE, 0, sizeof(cl_int)*_nElements * 6, LinkedElements, 0, NULL, NULL);
		ret = clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&LinkedElemenetsBuffer);
		ret = clSetKernelArg(kernelRK1, 2, sizeof(cl_mem), (void *)&LinkedElemenetsBuffer);
		ret = clSetKernelArg(kernelRK2, 2, sizeof(cl_mem), (void *)&LinkedElemenetsBuffer);
		ret = clSetKernelArg(kernelRK3, 2, sizeof(cl_mem), (void *)&LinkedElemenetsBuffer);
		ret = clSetKernelArg(kernelRK4, 2, sizeof(cl_mem), (void *)&LinkedElemenetsBuffer);

		cl_double* RadiusVectors = (cl_double*)_radiusVectors;
		ret = clEnqueueWriteBuffer(command_queue, RadiusVectorsBuffer, CL_FALSE, 0, vecStride * 6 * sizeof(cl_double), RadiusVectors, 0, NULL, NULL);
		ret = clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)&RadiusVectorsBuffer);
		ret = clSetKernelArg(kernelRK1, 3, sizeof(cl_mem), (void *)&RadiusVectorsBuffer);
		ret = clSetKernelArg(kernelRK2, 3, sizeof(cl_mem), (void *)&RadiusVectorsBuffer);
		ret = clSetKernelArg(kernelRK3, 3, sizeof(cl_mem), (void *)&RadiusVectorsBuffer);
		ret = clSetKernelArg(kernelRK4, 3, sizeof(cl_mem), (void *)&RadiusVectorsBuffer);

		if (allBoundaryForces == 0)
			allBoundaryForces = (double*)malloc(vecStride2 * sizeof(double)*_nElements);

		//1 ������� ������� �������� 0 ����������
		if (FreeDegrees == 0)
			FreeDegrees = (cl_uint*)malloc(6 * sizeof(cl_uint)*_nElements);


		for (int i = 0; i < _nElements; i++)
		{
			allBoundaryForces[i*vecStride2] = 0;
			allBoundaryForces[i*vecStride2 + 1] = 0;
			allBoundaryForces[i*vecStride2 + 2] = 0;
			allBoundaryForces[i*vecStride2 + 3] = 0;
			allBoundaryForces[i*vecStride2 + 4] = 0;
			allBoundaryForces[i*vecStride2 + 5] = 0;
			allBoundaryForces[i*vecStride2 + 6] = 0;
			allBoundaryForces[i*vecStride2 + 7] = 0;

			FreeDegrees[i * 6] = 1; FreeDegrees[i * 6 + 3] = 1;
			FreeDegrees[i * 6 + 1] = 1; FreeDegrees[i * 6 + 4] = 1;
			FreeDegrees[i * 6 + 2] = 1; FreeDegrees[i * 6 + 5] = 1;
			for (int c = 0; c < _boundaryParamsSet.size(); c++)
			{
				for (int n = 0; n < _boundaryParamsSet.at(c).GetNodesCount(); n++)
				{
					if (_boundaryParamsSet.at(c).GetNode(n) - 1 == i)
					{
						//Force
						if (_boundaryParamsSet.at(c).GetKind() == 3)
						{
							for (int p = 0; p < 3; p++)
							{
								double param = _boundaryParamsSet.at(c).GetParam(p);
								allBoundaryForces[i*vecStride2 + p] += param / _boundaryParamsSet.at(c).GetNodesCountInFullBoundary();
								param = _boundaryParamsSet.at(c).GetParam(p + 3);
								allBoundaryForces[i*vecStride2 + vecStride + p] += param / _boundaryParamsSet.at(c).GetNodesCountInFullBoundary();
							}
						}//sealed
						else if (_boundaryParamsSet.at(c).GetKind() == 4)
						{
							for (int p = 0; p < 3; p++)
							{
								if (_boundaryParamsSet.at(c).GetParam(p) < 0)
								{
									FreeDegrees[i * 6 + p] = 0;
									FreeDegrees[i * 6 + p + 3] = 0;
								}
								else
								{
									FreeDegrees[i * 6 + p] = 1;
									FreeDegrees[i * 6 + p + 3] = 1;
								}
							}
						}
					}
				}
			}
		}

		ret = clEnqueueWriteBuffer(command_queue, BoundaryForcesBuffer, CL_FALSE, 0, vecStride2 * sizeof(cl_double) * _nElements, allBoundaryForces, 0, NULL, NULL);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to write buffers.\n");
			exit(ret);
		}
		ret = clSetKernelArg(kernel, 4, sizeof(cl_mem), (void *)&BoundaryForcesBuffer);
		ret = clSetKernelArg(kernelRK1, 4, sizeof(cl_mem), (void *)&BoundaryForcesBuffer);
		ret = clSetKernelArg(kernelRK2, 4, sizeof(cl_mem), (void *)&BoundaryForcesBuffer);
		ret = clSetKernelArg(kernelRK3, 4, sizeof(cl_mem), (void *)&BoundaryForcesBuffer);
		ret = clSetKernelArg(kernelRK4, 4, sizeof(cl_mem), (void *)&BoundaryForcesBuffer);
		if (ret != 0)
		{
			fprintf(stderr, "Failed with args setup.\n");
			exit(ret);
		}
		ret = clEnqueueWriteBuffer(command_queue, BoundaryFixedBuffer, CL_FALSE, 0, 6 * sizeof(cl_int)*_nElements, FreeDegrees, 0, NULL, NULL);
		ret = clSetKernelArg(kernel, 5, sizeof(cl_mem), (void *)&BoundaryFixedBuffer);
		ret = clSetKernelArg(kernelRK1, 5, sizeof(cl_mem), (void *)&BoundaryFixedBuffer);
		ret = clSetKernelArg(kernelRK2, 5, sizeof(cl_mem), (void *)&BoundaryFixedBuffer);
		ret = clSetKernelArg(kernelRK3, 5, sizeof(cl_mem), (void *)&BoundaryFixedBuffer);
		ret = clSetKernelArg(kernelRK4, 5, sizeof(cl_mem), (void *)&BoundaryFixedBuffer);

		cl_double* ElementStressFactorCache = _elementStressFactorCache;
		ret = clEnqueueWriteBuffer(command_queue, ElementStressFactorCacheBuffer, CL_FALSE, 0, vecStride * sizeof(cl_double)*_nElements, ElementStressFactorCache, 0, NULL, NULL);
		ret = clSetKernelArg(kernel, 6, sizeof(cl_mem), (void *)&ElementStressFactorCacheBuffer);
		ret = clSetKernelArg(kernelRK1, 6, sizeof(cl_mem), (void *)&ElementStressFactorCacheBuffer);
		ret = clSetKernelArg(kernelRK2, 6, sizeof(cl_mem), (void *)&ElementStressFactorCacheBuffer);
		ret = clSetKernelArg(kernelRK3, 6, sizeof(cl_mem), (void *)&ElementStressFactorCacheBuffer);
		ret = clSetKernelArg(kernelRK4, 6, sizeof(cl_mem), (void *)&ElementStressFactorCacheBuffer);

		cl_double* StressScalingFactor = _stressScalingFactors;
		ret = clEnqueueWriteBuffer(command_queue, StressScalingFactorBuffer, CL_FALSE, 0, vecStride * sizeof(cl_double), StressScalingFactor, 0, NULL, NULL);
		ret = clSetKernelArg(kernel, 7, sizeof(cl_mem), (void *)&StressScalingFactorBuffer);
		ret = clSetKernelArg(kernelRK1, 7, sizeof(cl_mem), (void *)&StressScalingFactorBuffer);
		ret = clSetKernelArg(kernelRK2, 7, sizeof(cl_mem), (void *)&StressScalingFactorBuffer);
		ret = clSetKernelArg(kernelRK3, 7, sizeof(cl_mem), (void *)&StressScalingFactorBuffer);
		ret = clSetKernelArg(kernelRK4, 7, sizeof(cl_mem), (void *)&StressScalingFactorBuffer);

		cl_double* Coordinates = _coordinates;
		ret = clEnqueueWriteBuffer(command_queue, CoordinatesBuffer, CL_TRUE, 0, 3 * sizeof(cl_double)*_nElements, Coordinates, 0, NULL, NULL);
		ret = clSetKernelArg(kernel, 9, sizeof(cl_mem), (void *)&CoordinatesBuffer);
		ret = clSetKernelArg(kernelRK1, 9, sizeof(cl_mem), (void *)&CoordinatesBuffer);
		ret = clSetKernelArg(kernelRK2, 9, sizeof(cl_mem), (void *)&CoordinatesBuffer);
		ret = clSetKernelArg(kernelRK3, 9, sizeof(cl_mem), (void *)&CoordinatesBuffer);
		ret = clSetKernelArg(kernelRK4, 9, sizeof(cl_mem), (void *)&CoordinatesBuffer);
		/*	double _elasticModulus,			// ������ ��������� (������������ ������ ��� ���������� ����������)
		double _dampingFactorAngular,	// ����������� ����������� �������� �������������
		double _dampingFactorLinear,	// ����������� ����������� ��������� �������������
		double _elasticFactorLinear,	// ����������� ����������� �������� ���������
		double _elasticFactorAngular	// ����������� ����������� ������� ���������*/
		ret = clSetKernelArg(kernel, 10, sizeof(cl_double), (void *)&_elasticModulus);
		ret = clSetKernelArg(kernelRK1, 10, sizeof(cl_double), (void *)&_elasticModulus);
		ret = clSetKernelArg(kernelRK2, 10, sizeof(cl_double), (void *)&_elasticModulus);
		ret = clSetKernelArg(kernelRK3, 10, sizeof(cl_double), (void *)&_elasticModulus);
		ret = clSetKernelArg(kernelRK4, 10, sizeof(cl_double), (void *)&_elasticModulus);

		ret = clSetKernelArg(kernel, 11, sizeof(cl_double), (void *)&_dampingFactorAngular);
		ret = clSetKernelArg(kernelRK1, 11, sizeof(cl_double), (void *)&_dampingFactorAngular);
		ret = clSetKernelArg(kernelRK2, 11, sizeof(cl_double), (void *)&_dampingFactorAngular);
		ret = clSetKernelArg(kernelRK3, 11, sizeof(cl_double), (void *)&_dampingFactorAngular);
		ret = clSetKernelArg(kernelRK4, 11, sizeof(cl_double), (void *)&_dampingFactorAngular);

		ret = clSetKernelArg(kernel, 12, sizeof(cl_double), (void *)&_dampingFactorLinear);
		ret = clSetKernelArg(kernelRK1, 12, sizeof(cl_double), (void *)&_dampingFactorLinear);
		ret = clSetKernelArg(kernelRK2, 12, sizeof(cl_double), (void *)&_dampingFactorLinear);
		ret = clSetKernelArg(kernelRK3, 12, sizeof(cl_double), (void *)&_dampingFactorLinear);
		ret = clSetKernelArg(kernelRK4, 12, sizeof(cl_double), (void *)&_dampingFactorLinear);

		ret = clSetKernelArg(kernel, 13, sizeof(cl_double), (void *)&_elasticFactorLinear);
		ret = clSetKernelArg(kernelRK1, 13, sizeof(cl_double), (void *)&_elasticFactorLinear);
		ret = clSetKernelArg(kernelRK2, 13, sizeof(cl_double), (void *)&_elasticFactorLinear);
		ret = clSetKernelArg(kernelRK3, 13, sizeof(cl_double), (void *)&_elasticFactorLinear);
		ret = clSetKernelArg(kernelRK4, 13, sizeof(cl_double), (void *)&_elasticFactorLinear);

		ret = clSetKernelArg(kernel, 14, sizeof(cl_double), (void *)&_elasticFactorAngular);
		ret = clSetKernelArg(kernelRK1, 14, sizeof(cl_double), (void *)&_elasticFactorAngular);
		ret = clSetKernelArg(kernelRK2, 14, sizeof(cl_double), (void *)&_elasticFactorAngular);
		ret = clSetKernelArg(kernelRK3, 14, sizeof(cl_double), (void *)&_elasticFactorAngular);
		ret = clSetKernelArg(kernelRK4, 14, sizeof(cl_double), (void *)&_elasticFactorAngular);

		ret = clSetKernelArg(kernel, 15, sizeof(cl_int), (void*)&_nElements);
		ret = clSetKernelArg(kernelRK1, 15, sizeof(cl_int), (void*)&_nElements);
		ret = clSetKernelArg(kernelRK2, 15, sizeof(cl_int), (void*)&_nElements);
		ret = clSetKernelArg(kernelRK3, 15, sizeof(cl_int), (void*)&_nElements);
		ret = clSetKernelArg(kernelRK4, 15, sizeof(cl_int), (void*)&_nElements);

		ret = clSetKernelArg(kernel, 16, sizeof(cl_double), (void*)&_timeStep);
		ret = clSetKernelArg(kernelRK1, 16, sizeof(cl_double), (void*)&_timeStep);
		ret = clSetKernelArg(kernelRK2, 16, sizeof(cl_double), (void*)&_timeStep);
		ret = clSetKernelArg(kernelRK3, 16, sizeof(cl_double), (void*)&_timeStep);
		ret = clSetKernelArg(kernelRK4, 16, sizeof(cl_double), (void*)&_timeStep);
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::DownloadOpenCLBuffers()
	{
		//�� GPU
		cl_int ret = 0;
		ret = clEnqueueReadBuffer(command_queue, DataInternalBuffer, CL_TRUE, 0, 6 * sizeof(cl_double)*vecStride*_nElements, DataInternal, 0, NULL, NULL);

		ret = clEnqueueReadBuffer(command_queue, DataRotationMatrixBuffer, CL_TRUE, 0, matStride * sizeof(cl_double)*_nElements, DataRotationMatrix, 0, NULL, NULL);

		ret = clEnqueueReadBuffer(command_queue, StressBuffer, CL_TRUE, 0, 2 * vecStride * sizeof(cl_double)*_nElements, Stress, 0, NULL, NULL);

		ret = clEnqueueReadBuffer(command_queue, CoordinatesBuffer, CL_TRUE, 0, 3 * _nElements * sizeof(cl_double), _coordinates, 0, NULL, NULL);
	}


	//����� �������������� ����� MPI ���� �� ������� ������� ��������� � ������� ������������� �������� �� ��������������� ��������. �������� ��������, ��� ��� ����� ���������� ���, �������� - 
	//������ �������
	void Stress::StressStrainCppIterativeSolverOpenCL::DownloadSyncBuffers()
	{
		vector<vector<int>> table = *_gpuSyncTableProvider.GetSend();
		for (int i = 0; i < table.size(); i++)
		{
			cl_int ret = 0;

			ret = clEnqueueReadBuffer(command_queue, DataInternalBuffer, CL_TRUE, 6 * sizeof(cl_double) * vecStride * table[i][0], 6 * sizeof(cl_double) * vecStride * table[i][1], DataInternal, 0, NULL, NULL);

			ret = clEnqueueReadBuffer(command_queue, DataRotationMatrixBuffer, CL_TRUE, matStride * sizeof(cl_double) * table[i][0], matStride * sizeof(cl_double) * table[i][1], DataRotationMatrix, 0, NULL, NULL);

			ret = clEnqueueReadBuffer(command_queue, StressBuffer, CL_TRUE, 2 * vecStride * sizeof(cl_double) * table[i][0], 2 * vecStride * sizeof(cl_double)*table[i][1], Stress, 0, NULL, NULL);

			ret = clEnqueueReadBuffer(command_queue, CoordinatesBuffer, CL_TRUE, 3 * sizeof(cl_double) * table[i][0], 3 * sizeof(cl_double) * table[i][1], _coordinates, 0, NULL, NULL);
		

		}
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::UploadSyncBuffers()
	{
		vector<vector<int>> table = *_gpuSyncTableProvider.GetRecv();
		for (int i = 0; i < table.size(); i++)
		{
			cl_int ret = 0;

			ret = clEnqueueWriteBuffer(command_queue, DataInternalBuffer, CL_TRUE, 6 * sizeof(cl_double) * vecStride * table[i][0], 6 * sizeof(cl_double) * vecStride * table[i][1], DataInternal, 0, NULL, NULL);

			ret = clEnqueueWriteBuffer(command_queue, DataRotationMatrixBuffer, CL_TRUE, matStride * sizeof(cl_double) * table[i][0], matStride * sizeof(cl_double) * table[i][1], DataRotationMatrix, 0, NULL, NULL);

			ret = clEnqueueWriteBuffer(command_queue, StressBuffer, CL_TRUE, 2 * vecStride * sizeof(cl_double) * table[i][0], 2 * vecStride * sizeof(cl_double)*table[i][1], Stress, 0, NULL, NULL);

			ret = clEnqueueWriteBuffer(command_queue, CoordinatesBuffer, CL_TRUE, 3 * sizeof(cl_double) * table[i][0], 3 * sizeof(cl_double) * table[i][1], _coordinates, 0, NULL, NULL);
		}
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::Solve(const int nIterations)
	{


#ifndef MPI_DECOMPOSITION

		//	Solve(nIterations);
		_iterationNumber = 0;
		_testTimer.Start(0);
		while (_iterationNumber != nIterations && _rotationSolver->IsValid())
		{
			_iterationNumber++;
			_nIteration++;

			pcClCalculate.Start();
			clCalculate(kernel);
			pcClCalculate.Stop();		
		}

		char pcClCalculateText[128];
		sprintf(pcClCalculateText, "ClCalculate Total: ");
		pcClCalculate.Print(pcClCalculateText, false);
		_testTimer.Stop(0);

		
		pcDownloadOpenCLBuffers.Start();
		DownloadOpenCLBuffers();
		pcDownloadOpenCLBuffers.Stop();
		char text[128];
		sprintf(text, "OPENCL AAA Total: ");
		pcDownloadOpenCLBuffers.Print(text, false);

		// DownloadOpenCLBuffers();
#else
		SolveMPI(nIterations);
#endif

	}

#ifndef DIRECT_INT
	// MPI-������
	void StressStrainCppIterativeSolverOpenCL::SolveMPI(const int nIterations)
	{
		_MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);

		int rank, size;
		MPI_Comm_size(MPI_COMM_WORLD, &size);
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);


		// ���������� ������� ��� ������
		// int counts[20], displs[20]{ 0 };
		// MPI_Datatype sTypes[20], rTypes[20];

		int *counts = new int[size];
		int *displs = new int[size];
		MPI_Datatype *sTypes = new MPI_Datatype[size];
		MPI_Datatype *rTypes = new MPI_Datatype[size];
	
		// std::cout << "DEBUG: nElements openCl " << _nElements << "DEBUGEND" << endl;

		for (int i = 0; i < size; i++) 
		{
			displs[i] = 0;
			counts[i] = 0;
		}
		
		pcSync.Start();
		_syncTableGpu.CalculateMPIDataTypes(rank, size, counts, displs, sTypes, rTypes);

		// for (int i = 0; i < size; i++) 
		// {
		// 	std::cout << rank << "OPENCL SOLVER: displs[" << i << "] = " << displs[i] << "; counts[" << i << "] = " << counts[i] << std::endl;
		// }

		// for (int i = 0; i < size; i++) 
		// {
		// 	std::cout << rank << "OPENCL SOLVER BEFORE SYNC: _dataInternal[" << i << "] = " << _dataInternal[i] << std::endl;
		// }

		// ��������� �������������
		MPI_Alltoallw(_dataInternal, counts, displs, sTypes, _dataInternal, counts, displs, rTypes, MPI_COMM_WORLD);

		// for (int i = 0; i < size; i++) 
		// {
		// 	std::cout << rank << "OPENCL SOLVER AFTER 0 SYNC: _dataInternal[" << i << "] = " << _dataInternal[i] << std::endl;
		// }

		UploadAllOpenCLBuffers();
		pcSync.Stop();

		_iterationNumber = 0;
		_testTimer.Start(0);

		
		while (_iterationNumber != nIterations && _rotationSolver->IsValid())
		{
			_iterationNumber++;
			_nIteration++;

			// std::cout << "DEBUG: nElements openCl " << _nElements << "DEBUGEND" << endl;

			_MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);

			_testTimer.Start(3);
			pcClCalculate.Start();

			clCalculate(kernelRK1);

			pcClCalculate.Stop();
			_testTimer.Stop(3);

			// ������������� ��-1
			
			DownloadSyncBuffers();pcSync.Start();
			MPI_Alltoallw(_dataInternal, counts, displs, sTypes, _dataInternal, counts, displs, rTypes, MPI_COMM_WORLD);
			pcSync.Stop();UploadSyncBuffers();

			// for (int i = 0; i < size; i++) 
			// {
			// 	std::cout << rank << "OPENCL SOLVER AFTER 1 SYNC: _dataInternal[" << i << "] = " << _dataInternal[i] << std::endl;
			// }
			
			
			_testTimer.Start(3);
			pcClCalculate.Start();

			clCalculate(kernelRK2);

			pcClCalculate.Stop();
			_testTimer.Stop(3);

			// ������������� ��-2
			
			DownloadSyncBuffers();pcSync.Start();
			MPI_Alltoallw(_dataInternal, counts, displs, sTypes, _dataInternal, counts, displs, rTypes, MPI_COMM_WORLD);
			pcSync.Stop();UploadSyncBuffers();

			// for (int i = 0; i < size; i++) 
			// {
			// 	std::cout << rank << "OPENCL SOLVER AFTER 2 SYNC: _dataInternal[" << i << "] = " << _dataInternal[i] << std::endl;
			// }
			

			_testTimer.Start(3);
			pcClCalculate.Start();

			clCalculate(kernelRK3);

			pcClCalculate.Stop();
			_testTimer.Stop(3);

			// ������������� ��-3
			
			DownloadSyncBuffers();pcSync.Start();
			MPI_Alltoallw(_dataInternal, counts, displs, sTypes, _dataInternal, counts, displs, rTypes, MPI_COMM_WORLD);
			pcSync.Stop();UploadSyncBuffers();

			// for (int i = 0; i < size; i++) 
			// {
			// 	std::cout << rank << "OPENCL SOLVER AFTER 3 SYNC: _dataInternal[" << i << "] = " << _dataInternal[i] << std::endl;
			// }
			

			_testTimer.Start(3);
			pcClCalculate.Start();

			clCalculate(kernelRK4);

			pcClCalculate.Stop();
			_testTimer.Stop(3);

			// ������������� ��-4
	
			DownloadSyncBuffers();
			pcSync.Start();
			MPI_Alltoallw(_dataInternal, counts, displs, sTypes, _dataInternal, counts, displs, rTypes, MPI_COMM_WORLD);
			pcSync.Stop();
			UploadSyncBuffers();

			// for (int i = 0; i < size; i++) 
			// {
			// 	std::cout << rank << "OPENCL SOLVER AFTER 4 SYNC: _dataInternal[" << i << "] = " << _dataInternal[i] << std::endl;
			// }
			
		}
		_testTimer.Stop(0);

		// std::cout << "TRANSFER TIME: " << _testTimer.Get(0) - _testTimer.Get(3) << endl;
		
		// pcDownloadOpenCLBuffers.Start();
		DownloadOpenCLBuffers();
		// pcDownloadOpenCLBuffers.Stop();
		// char text[128];
		// sprintf(text, "OPENCL DownloadOpenCLBuffers Total: ");
		// pcDownloadOpenCLBuffers.Print(text, false);

		char pcClCalculateText[128];
		sprintf(pcClCalculateText, "ClCalculate Total: ");
		pcClCalculate.Print(pcClCalculateText, false);

		char pcSyncText[128];
		sprintf(pcSyncText, "Model Sync Total: ");
		pcSync.Print(pcSyncText, false);

		delete[] counts;
		delete[] displs;
		delete[] rTypes;
		delete[] sTypes;
	}
#endif

	void Stress::StressStrainCppIterativeSolverOpenCL::clCalculate(cl_kernel currentKernel)
	{
		cl_int ret = 0;
		size_t max_all_work_items = 256;
		/*for (size_t i = 0; i < _nElements + max_all_work_items; i += max_all_work_items)
		{
			size_t offset = i;
			size_t count = 0;

			max_all_work_items = min(max_all_work_items, _nElements);

			if ((ret = clEnqueueNDRangeKernel(command_queue, currentKernel, 1, (size_t*)&offset, (size_t*)&max_all_work_items, (size_t*)&max_all_work_items, 0, NULL, NULL)) != 0)
			{
				fprintf(stderr, "Failed to execute kernels.\n");
				std::cout<<ret;
				exit(ret);
			}
		}	*/		
		size_t offset = 0;
		size_t count = 0;
		size_t local = 256;
		//костыль - не верно
		size_t global = _nElements - _nElements % local;
		if ((ret = clEnqueueNDRangeKernel(command_queue, currentKernel, 1, (size_t*)&offset, (size_t*)&global, (size_t*)&local, 0, NULL, NULL)) != 0)
		{
			fprintf(stderr, "Failed to execute kernels.\n");
			std::cout<<ret;
			exit(ret);
		}
	    clFinish(command_queue);
	}

	void StressStrainCppIterativeSolverOpenCL::ApplyBoundary()
	{
		//�������� � ������ �� frm_provider
		//BCT_stresstrainBoundaryForce = 3,
		//BCT_stresstrainBoundarySealing = 4,

		double* accelerationsPointer = GetElementAcceleration(0);
		vector<BoundaryParams>::iterator it = _boundaryParamsSet.begin();

		while (it != _boundaryParamsSet.end())
		{
			switch (it->GetKind())
			{
			case 3:
				it->ApplyForceBoundary(accelerationsPointer);
				break;

			case 4:
				it->ApplySealedBoundary(accelerationsPointer);
				break;

			default:
				break;
			}
			it++;
		}
	}

	void StressStrainCppIterativeSolverOpenCL::ApplyMass()
	{
#ifdef _DEBUG
		//_controlfp(0, EM_ZERODIVIDE);
		//_control87(~_EM_ZERODIVIDE, _MCW_EM);
#endif

		//double* accelerations = GetElementAcceleration(0); // debug
		//std::cout << "Num threads = " << _numThreads << std::endl;
#ifdef OMP_SOLVE
#pragma omp parallel for num_threads(_numThreads)
#endif
		for (int elementId = 0; elementId < _nElements; elementId++)
		{
			MakeVec3(GetElementAcceleration(elementId)) /= _cellMass;
			MakeVec3(GetElementAccelerationAngular(elementId)) /= _cellInertia;
		}
	}


	void Stress::StressStrainCppIterativeSolverOpenCL::clInitKernel()
	{
		cl_program program;
		cl_int ret = 0;
		FILE *fp;
		const char fileName[] = "../../Solvers/Stress/OpenCLSolverKernels.cl";		
		size_t source_size;
		char *source_str;

		try {
			fp = fopen(fileName, "r");
			if (!fp) {
				fprintf(stderr, "Failed to load kernel.\n");
				exit(ret);
			}
			source_str = (char *)malloc(MAX_SOURCE_SIZE);
			memset(source_str, 0, sizeof(char)*MAX_SOURCE_SIZE);
			source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
			fclose(fp);
		}
		catch (int a) {
			printf("%d", a);
		}

		program = clCreateProgramWithSource(clContext, 1, (const char **)&source_str, &source_size, &ret);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to create program with source.\n");
			exit(ret);
		}
		puts("Building OpenCLSolverKernels.cl...");
		char args[200] = "-cl-std=CL1.2";
		ret = clBuildProgram(program, 1, &deviceId, args, NULL, NULL);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to build kernel program.\n");
			if (ret == CL_BUILD_PROGRAM_FAILURE) {
				//����������� ������� ����
				size_t log_size;
				clGetProgramBuildInfo(program, deviceId, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
				//��������� ������ ��� ���
				char *log = (char *)malloc(log_size);
				//���������� ����
				clGetProgramBuildInfo(program, deviceId, CL_PROGRAM_BUILD_LOG, log_size, log, NULL);

				fprintf(stderr, "%s\n", log);
			}
			exit(ret);
		}
		puts("Done");
		kernel = clCreateKernel(program, "clCalculate", &ret);
		kernelRK1 = clCreateKernel(program, "clSyncCalculateRK1", &ret);
		kernelRK2 = clCreateKernel(program, "clSyncCalculateRK2", &ret);
		kernelRK3 = clCreateKernel(program, "clSyncCalculateRK3", &ret);
		kernelRK4 = clCreateKernel(program, "clSyncCalculateRK4", &ret);

		if (ret != 0)
		{
			fprintf(stderr, "Failed to create kernel\n");
			exit(ret);
		}
		command_queue = clCreateCommandQueue(clContext, deviceId, 0, &ret);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to create command queue.\n");
			exit(ret);
		}
		device_info = CL_DEVICE_MAX_WORK_ITEM_SIZES;
		size_t parameter[3] = { 0 };
		size_t fact_size = 0;
		ret = clGetDeviceInfo(deviceId, device_info, sizeof(size_t) * 3, (void*)parameter, &fact_size);
		if (ret != 0)
		{
			fprintf(stderr, "Failed to getting gevice info.\n");
			exit(ret);
		}
		max_all_work_items = _nElements;
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::SolveFull(const int nIterations)
	{
		Solve(nIterations);
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::Solve1()
	{
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::Solve2()
	{
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::Solve3()
	{
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::Solve4()
	{
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::Solve5()
	{
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::GetDisplacement(float * data)
	{

		this->StressStrainCppIterativeSolver::GetDisplacement(data);
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::GetStressesByFirstTheoryOfStrength(float * data)
	{
		this->StressStrainCppIterativeSolver::GetStressesByFirstTheoryOfStrength(data);
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::GetStressesByVonMises(float * data)
	{
		this->StressStrainCppIterativeSolver::GetStressesByVonMises(data);
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::CalculateStrains(size_t side, double * shiftStrains, double * velocityStrains, size_t nodeId1, size_t nodeId2) const
	{
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::CalculateStrainsUa(size_t side, double * shiftStrains, double * velocityStrains, size_t nodeId1, size_t nodeId2) const
	{
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::GetStressesX(float * data)
	{
		this->StressStrainCppIterativeSolver::GetStressesX(data);
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::GetStressesY(float * data)
	{
		this->StressStrainCppIterativeSolver::GetStressesY(data);
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::GetStressesZ(float * data)
	{
		this->StressStrainCppIterativeSolver::GetStressesZ(data);
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::GetStressesXY(float * data)
	{
		this->StressStrainCppIterativeSolver::GetStressesXY(data);
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::GetStressesXZ(float * data)
	{
		this->StressStrainCppIterativeSolver::GetStressesXZ(data);
	}

	void Stress::StressStrainCppIterativeSolverOpenCL::GetStressesYZ(float * data)
	{
		this->StressStrainCppIterativeSolver::GetStressesYZ(data);
	}

	double3 Stress::operator-(double3 a, double3 b)
	{
		double3 result;
		result.x = a.x - b.x;
		result.y = a.y - b.y;
		result.z = a.z - b.z;
		return result;
	}

	double3 Stress::operator+(double3 a, double3 b)
	{
		double3 result;
		result.x = a.x + b.x;
		result.y = a.y + b.y;
		result.z = a.z + b.z;
		return result;
	}

	double3 Stress::operator-(double3 a)
	{
		double3 result;
		result.x = -a.x;
		result.y = -a.y;
		result.z = -a.z;
		return result;
	}
}
