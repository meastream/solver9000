#pragma once

#include "TestSyncTable.h"
#include "mpi.h"

static class MPISynchronisation
{
public:
	MPISynchronisation(){ }
	~MPISynchronisation(){}

	static void AlltoallvSynchronize(int rank, int size, TestSyncTable syncTable, double* myData);
	static void AlltoallwSynchronize(int rank, int size, TestSyncTable syncTable, double* myData);
	static void SendrecvSynchronize(int rank, int size, TestSyncTable syncTable, double* myData);
	static void SendRecvSynchronize(int rank, int size, TestSyncTable syncTable, double* myData);
};

