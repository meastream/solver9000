#include "PThreadsManager.h"

PThreadsManager::PThreadsManager(int num, SyncTable* table):
_threadsNumber(num),
_table(table)
{
	_threads.resize(num);
	sendBuffers.resize(num);
	recvBuffers.resize(num);
	for(int i = 0; i < num; i++)
	{
		if(table->CountSend(i) > 0)
			sendBuffers[i] = new double[table->CountSend(i)];
		if(table->CountRecv(i) > 0)
			recvBuffers[i] = new double[table->CountRecv(i)];
	}
	AddBariers(2); // для AllToAll
}

void PThreadsManager::Wait()
{
	for(int i = 0; i < _threadsNumber; i++)
		pthread_join(_threads[i],NULL);
}

void PThreadsManager::RunTask(int threadNumber)
{
	if(threadNumber >= _threadsNumber)
		throw "Allocated threads number too small";

	// TODO: обработать значение, возвращаемое pthread_create
	int res = pthread_create
		(
			&_threads[threadNumber],
			NULL,
			_threadsData[threadNumber]->_task,
			_threadsData[threadNumber]
		);

	(void)res;
}

void PThreadsManager::Barrier(int id)
{ 
	if(id >= _barriers.size())
		throw "Not enough barriers";
	
	_barriers[id]->WaitFor(_threadsNumber);
}

void PThreadsManager::GlobalBarrier(int id)
{ 
	if(id >= _barriers.size())
		throw "Not enough barriers";
	
	_barriers[id]->WaitFor(_threadsNumber+1);
}

PThreadsManager::~PThreadsManager()
{
	for(int i = 0; i < _threadsNumber; i++)
	{
		if(_table->CountSend(i) > 0)
			delete [] sendBuffers[i];
		if(_table->CountRecv(i) > 0)
			delete [] recvBuffers[i];
	}
	for(int i = 0; i < _barriers.size(); i++)
		delete _barriers[i];

	for(int i = 0; i < _threadsData.size(); i++)
		delete _threadsData[i];
}

void PThreadsManager::Pack(int id, double const*data)
{
	if(id >= _threadsNumber)
		throw "Allocated threads number too small";

	_table->Pack(id, data, sendBuffers[id]);
}

void PThreadsManager::Unpack(int id, double *data)
{
	if(id >= _threadsNumber)
		throw "Allocated threads number too small";

	_table->Unpack(id, data, recvBuffers[id]);
}

void PThreadsManager::AddTask(int threadNumber, int iterationsNumber, void *(*task)(void *))
{
	if(threadNumber >= _threadsNumber)
		throw "Allocated threads number too small";

	_threadsData.push_back(new PThreadData(threadNumber, iterationsNumber, task, this));
}

void PThreadsManager::AddBariers(int barriersNumber)
{
	for (int i = 0; i < barriersNumber; i++)
		_barriers.push_back(new BarrierData());
}

void PThreadsManager::AllToAll()
{
	GlobalBarrier(0);
	_table->Exchange(sendBuffers, recvBuffers);
	GlobalBarrier(1);
}
