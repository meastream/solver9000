#include "SyncTable.h"
#include "TestSyncTable.h"
#include "mpi.h"
#include <iostream>

SyncTable::~SyncTable(void)
{
}

SyncTable::SyncTable(int nProcs)
{
	Allocate(nProcs);
}

void SyncTable::Allocate(int nProcs)
{
	_table.resize(nProcs);
	_offsetsTable.resize(nProcs);
	ExchangeRowIterator it;
	for (ExchangeRowIterator it = _table.begin(); it != _table.end(); ++it)
		it->resize(nProcs);
	for (vector<vector<ExchangeBlock> >::iterator it = _offsetsTable.begin(); it != _offsetsTable.end(); ++it)
		it->resize(nProcs);
}
  
int SyncTable::CountNonzeroBlocks() const
{	
	int counter = 0;
	for(int i = 0; i < _table.size(); i++)
		for(int j = 0; j < _table[i].size(); j++)
			if(!_table[i][j].IsEmpty())
				counter++;
	return counter;
}

int SyncTable::CountSend(int rowId) const
{
	int count = 0;
	for (ExchangeColCIterator it = _table[rowId].begin(); it != _table[rowId].end(); ++it)
		count += it->CountSend();
	return count;
}

int SyncTable::CountRecv(int rowId) const
{
	int count = 0;
	for (ExchangeColCIterator it = _table[rowId].begin(); it != _table[rowId].end(); ++it)
		count += it->CountRecv();
	return count;
}

void SyncTable::GetOffsets(int i, int j, vector<pair<int, int> > &blocksSend, 
	vector<pair<int, int> > &blocksRecv)
{
	_table[i][j].GetOffsets(blocksSend, blocksRecv);
}

void SyncTable::OneToAll
	(
		int subsolverId,
		const double* sendBuffer,
		vector<double*>& recvBuffers
	)
{
	for (int i = 0; i < _table.size(); i++)
	{
		const ExchangeBlock& block = _offsetsTable[i][subsolverId];

		if (block.size != 0)
		{
			memcpy
				(
					recvBuffers[i] + block.offsetTo,
					sendBuffer + block.offsetFrom,
					block.size * sizeof(double)
				);
		}
	}
}

// TODO: разобраться, что за неиспользуемый параметр nodesCount
void SyncTable::ConvertTo
	(
		int freedomsCount,
		int /* nodesCount */,
		SyncTable& addressesSyncTable
	)	const
{
	addressesSyncTable.Allocate(CountParts());
	for (int i = 0; i < _table.size(); i++)
	{
		for (int j = 0; j < _table[i].size(); j++)
		{
			const ExchangePair& exchangePair = _table[i][j];
			ExchangePair& addressesExchangePair = addressesSyncTable._table[i][j];

			if (!exchangePair.IsEmpty())
			{
				exchangePair.ConvertTo(freedomsCount, addressesExchangePair);
			}
		}
	}
}

int* SyncTable::GetSendOffsets(int i, int j)
{
	int size = this->_table[i][j].CountSend();
	int* sends = new int[size];
	for (int k = 0; k < size; k++)
	{
		sends[k] = this->_table[i][j].GetSend(k) * 8;
	}
	return sends;
}

int* SyncTable::GetRecvOffsets(int i, int j)
{
	int size = this->_table[i][j].CountRecv();
	int* recvs = new int[size];
	for (int k = 0; k < size; k++)
	{
		recvs[k] = this->_table[i][j].GetRecv(k) * 8;
	}
	return recvs;
}

int* SyncTable::GetSendLengths(int i, int j)
{
	int size = _table[i][j].CountSend();
	int* counts = new int[size];
	for (int k = 0; k < size; k++)
	{
		counts[k] = 8;
	}
	return counts;
}

int* SyncTable::GetRecvLengths(int i, int j)
{
	int size = _table[i][j].CountRecv();
	int* counts = new int[size];
	for (int k = 0; k < size; k++)
	{
		counts[k] = 8;
	}
	return counts;
}

// Необходимо контролировать размер передаваемых массивов counts, displs, sTypes и rTypes, т.к. в случае, когда
// количество процессов будет превосходить размерность массивов, возникнет ошибка invalid datatype при вызове MPI_Alltoallw()
// TODO: Размер массивов можно и даже нужно не указывать!
void SyncTable::CalculateMPIDataTypes(int rank, int size, int *counts, int *displs, MPI_Datatype *sTypes, MPI_Datatype *rTypes)
{
	for (int i = 0; i < size; i++)
	{
		// Если есть совместная грань
		if (_table[rank][i].CountRecv() > 0)
		{
			// Регистрируем тип данных для отправки
			MPI_Datatype sendType;
			int countSend = _table[rank][i].CountSend();
			int *offsets = GetSendOffsets(rank, i);
			int *lengths = GetSendLengths(rank, i);
			MPI_Type_indexed(countSend, lengths, offsets, MPI_DOUBLE, &sendType);
			MPI_Type_commit(&sendType);
			

			// Регистрируем тип данных для приема
			MPI_Datatype recvType;
			MPI_Type_indexed(_table[rank][i].CountRecv(), GetRecvLengths(rank, i), GetRecvOffsets(rank, i), MPI_DOUBLE, &recvType);
			MPI_Type_commit(&recvType);

			// Сохраняем изменения
			counts[i] = 1;
			sTypes[i] = sendType;
			rTypes[i] = recvType;
		}
		else
		{
			// Оставляем поля пустыми
			counts[i] = 0;
			sTypes[i] = MPI_CHAR;
			rTypes[i] = MPI_CHAR;
		}
	}
}