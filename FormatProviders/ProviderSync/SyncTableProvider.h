#ifndef SyncTableProviderH
#define SyncTableProviderH

#include <string> 
#include <vector> 
#include <fstream>

#include "SyncTable.h"

using std::string;
using std::vector;
using std::ofstream;
using std::ifstream;
using std::endl;

void ParseBlock(const string& block, int& offset, int& count);

/**
* Класс для чтения файла с таблицей синхронизации
*
* структура файла
* nParts - количество частей
* part1 part2 nblocks offset:size
* nVertices in full model
* [partId for vertex 0] ... [partId for vertex nVertices]
*
* @author Getmanskiy Victor
*/
class SyncTableProvider
{
private:
	SyncTable _table;
	int _bodyId;
	int _solverId;
public:
	SyncTableProvider() :_bodyId(0), _solverId(0) {};
	void Init(const SyncTable& table, int bodyId, int solverId);

	void Read(const string& fileName);
	void Write(const string& fileName);

	SyncTable& GetTable() { return _table; }
	int GetBodyId() const { return _bodyId; }
	int GetSolverId() const { return _solverId; }
};

/*****
Специально для решателя на графическом ускорителе. Служит для транспорта между GPU и данными в ОЗУ
*/
class GpuSyncTableProvider
{
private:
	vector<vector<int>> _send;
	vector<vector<int>> _recieve;
	int _rank;

public:
	GpuSyncTableProvider() :_rank(0) {};

	GpuSyncTableProvider(int rank)
	{ _rank = rank; }

	void Read(const string& fileName);

	//table[counter][0] offset for current subnet
	//table[counter][1] count for current subnet
	vector<vector<int>>* GetSend() {
		return &_send;
	}

	//table[counter][0] offset for current subnet
	//table[counter][1] count for current subnet
	vector<vector<int>>* GetRecv() {
		return &_recieve;
	}
};

#endif