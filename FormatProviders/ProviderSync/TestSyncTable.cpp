#include "TestSyncTable.h"

Table TestSyncTable::SmallTestInit(int countSubmodels)
{
	// ��������� ������ ��� �������
	Table sendTable;
	sendTable.resize(countSubmodels);
	for (int i = 0; i < countSubmodels; i++)
	{
		sendTable[i].resize(countSubmodels);
	}

	// ������������� �� �������
	// ������ ������
	sendTable[0][0].resize(0);	// 1-1
	sendTable[0][3].resize(0);	// 1-4
	sendTable[1][1].resize(0);	// 2-2
	sendTable[1][2].resize(0);	// 2-3
	sendTable[2][1].resize(0);	// 3-2
	sendTable[2][2].resize(0);	// 3-3
	sendTable[3][0].resize(0);	// 4-1
	sendTable[3][3].resize(0);	// 4-4

	// 1-2
	sendTable[0][1].push_back(ExchangeBlock(2, 0, 6));
	sendTable[0][1].push_back(ExchangeBlock(6, 4, 6));
	sendTable[0][1].push_back(ExchangeBlock(10, 8, 6));

	// 1-3
	sendTable[0][2].push_back(ExchangeBlock(8, 0, 18));

	// 2-1
	sendTable[1][0].push_back(ExchangeBlock(1, 3, 6));
	sendTable[1][0].push_back(ExchangeBlock(5, 7, 6));
	sendTable[1][0].push_back(ExchangeBlock(9, 11, 6));

	// 2-4
	sendTable[1][3].push_back(ExchangeBlock(9, 0, 18));

	// 3-1
	sendTable[2][0].push_back(ExchangeBlock(3, 12, 18));

	// 3-4
	sendTable[2][3].push_back(ExchangeBlock(5, 3, 6));
	sendTable[2][3].push_back(ExchangeBlock(9, 7, 6));

	// 4-2
	sendTable[3][1].push_back(ExchangeBlock(4, 12, 18));

	// 4-3
	sendTable[3][2].push_back(ExchangeBlock(4, 6, 6));
	sendTable[3][2].push_back(ExchangeBlock(8, 10, 6));

	return sendTable;
}

Table TestSyncTable::BigTestInit(int countSubmodels)
{
	// ��������� ������ ��� �������
	Table sendTable;
	sendTable.resize(4);
	for (int i = 0; i < 4; i++)
	{
		sendTable[i].resize(4);
	}

	// ������������� �� �������
	// ������ ������
	sendTable[0][0].resize(0);	// 1-1
	sendTable[0][3].resize(0);	// 1-4
	sendTable[1][1].resize(0);	// 2-2
	sendTable[1][2].resize(0);	// 2-3
	sendTable[2][1].resize(0);	// 3-2
	sendTable[2][2].resize(0);	// 3-3
	sendTable[3][0].resize(0);	// 4-1
	sendTable[3][3].resize(0);	// 4-4

	for (int i = 0; i < 20; i++)
	{
		// 1-2
		sendTable[0][1].push_back(ExchangeBlock(i*20+18, i*20+0, 6));
		// 2-1
		sendTable[1][0].push_back(ExchangeBlock(i*20+1, i*20+19, 6));
		// 3-4
		sendTable[2][3].push_back(ExchangeBlock(i * 20 + 18, i * 20 + 0, 6));
		// 4-3
		sendTable[3][2].push_back(ExchangeBlock(i * 20 + 1, i * 20 + 19, 6));
	}

	// 1-3
	sendTable[0][2].push_back(ExchangeBlock(20*18, 0, 6*20));

	// 2-4
	sendTable[1][3].push_back(ExchangeBlock(20 * 18, 0, 6 * 20));

	// 3-1
	sendTable[2][0].push_back(ExchangeBlock(20, 20*19, 6*20));

	// 4-2
	sendTable[3][1].push_back(ExchangeBlock(20, 20 * 19, 6 * 20));

	return sendTable;
}

void TestSyncTable::PackSendData(int proc, double* data, double* pack, int* displs, int* counts)
{
	displs[0] = 0;
	int index = 0;

	for (int i = 0; i < _table.size(); i++)
	{
		int size = _table[proc][i].size();

		if (size != 0)
		{
			counts[i] = 0;
			displs[i] = index;
			for (int k = 0; k < size; k++)
			{
				memcpy(pack + index, data+_table[proc][i][k].offsetFrom*6, _table[proc][i][k].size*sizeof(double));
				counts[i] += _table[proc][i][k].size;
				index += _table[proc][i][k].size;
			}
		}
		else
		{
			counts[i] = 0;
			displs[i] = index;
		}
	}
}

void TestSyncTable::UnpackRecvData(int proc, double* data, double* pack, int* displs)
{
	int index = 0;

	for (int i = 0; i < _table.size(); i++)
	{
		int size = _table[i][proc].size();

		if (size != 0)
		{
			index = displs[i];
			for (int k = 0; k < size; k++)
			{
				memcpy(data + _table[i][proc][k].offsetTo * 6, pack + index, _table[i][proc][k].size*sizeof(double));
				index += _table[i][proc][k].size;
			}
		}
	}
}

int* TestSyncTable::GetFromOffsets(int i, int j)
{
	int size = _table[i][j].size();
	int* sends = new int[size];
	for (int k = 0; k < size; k++)
	{
		sends[k] = _table[i][j][k].offsetFrom * 6;
	}
	return sends;
}

int* TestSyncTable::GetToOffsets(int i, int j)
{
	int size = _table[i][j].size();
	int* recvs = new int[size];
	for (int k = 0; k < size; k++)
	{
		recvs[k] = _table[i][j][k].offsetTo * 6;
	}
	return recvs;
}

int* TestSyncTable::GetCounts(int i, int j)
{
	int size = _table[i][j].size();
	int* counts = new int[size];
	for (int k = 0; k < size; k++)
	{
		counts[k] = _table[i][j][k].size;
	}
	return counts;
}
