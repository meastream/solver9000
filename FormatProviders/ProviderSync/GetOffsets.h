#ifndef GetOffsetsH
#define GetOffsetsH

#include <vector>
using std::vector;
using std::pair;

void GetOffsets(const vector<int>& data, vector<pair<int, int> >& blocks);

#endif