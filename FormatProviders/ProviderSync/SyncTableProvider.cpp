//#include "../../Fcore/fcore.h"
#include "SyncTableProvider.h" 
#include "../../Test/Wrappers/StringRoutines.h"

#include <fstream>
#include <string>
#include <iostream>

using std::ifstream;
using std::ofstream;
using std::string;
using std::endl;

void ParseBlock(const string& block, int& offset, int& count)
{
	int pos = block.find(":");
	if (pos != string::npos)
	{
		offset = StringToNumber<int>(block.substr(0, pos));
		count = StringToNumber<int>(block.substr(pos + 1, block.length() - pos));
	}
	else
	{
		offset = StringToNumber<int>(block);
		count = 1;
	}
}

void SyncTableProvider::Init(const SyncTable& table, int bodyId, int solverId)
{
	_bodyId = bodyId;
	_solverId = solverId;
	_table = table;
}


void SyncTableProvider::Read(const string& fileName)
{
	ifstream ifs(fileName.c_str());
	if (ifs.is_open())
	{
		int nParts, nAdjs, nodeFrom, nodeTo, nSendBlocks, nRecvBlocks;

		ifs >> nParts >> _bodyId >> _solverId >> nAdjs;
		_table.Allocate(nParts);
		for (int j = 0; j < nAdjs; j++)
		{
			ifs >> nodeFrom >> nodeTo >> nSendBlocks >> nRecvBlocks;
			string block;
			int offset, count;
			// sends
			for (int i = 0; i < nSendBlocks; i++)
			{
				ifs >> block;
				ParseBlock(block, offset, count);
				//
				_table[nodeFrom][nodeTo].AddSendOffsets(offset, count);
			}
			for (int i = 0; i < nRecvBlocks; i++)
			{
				ifs >> block;
				ParseBlock(block, offset, count);
				//
				_table[nodeFrom][nodeTo].AddRecvOffsets(offset, count);
			}
		}
		ifs.close();
	}
	else
	{
		printf("Exception!!! File not opened: ", fileName.c_str());
	}
}

/* ����� ������� �������������:
���������� ����������
����� ���������, ��� ��������
���������� ��������������� ��������
(������ ������� ����������, ���������� ������� �/�
������ �������� : ���������� ���������
������ ������ : ���������� ���������) ...
*/
void SyncTableProvider::Write(const string& fileName)
{
	ofstream ofs(fileName.c_str());
	if (ofs.is_open())
	{
		vector<pair<int, int> > sendOffsets;
		vector<pair<int, int> > recvOffsets;

		ofs << _table.CountParts() << endl
			<< _bodyId << ' '
			<< _solverId << endl
			<< _table.CountNonzeroBlocks() << endl;
		for (int i = 0; i < _table.CountParts(); i++)
		{
			for (int j = 0; j < _table.CountParts(); j++)
			{
				if (_table[i][j].IsEmpty())
					continue;

				_table.GetOffsets(i, j, sendOffsets, recvOffsets);
				ofs << i << ' ' << j << ' ' << sendOffsets.size() << ' ' << recvOffsets.size() << endl;
				for (int k = 0; k < sendOffsets.size(); k++)
				{
					ofs << ' ' << sendOffsets[k].first << ':' << sendOffsets[k].second;
				}
				ofs << endl;
				for (int k = 0; k < recvOffsets.size(); k++)
				{
					ofs << ' ' << recvOffsets[k].first << ':' << recvOffsets[k].second;
				}
				ofs << endl;
			}
		}
	}
	else
	{
		printf("Exception!!! File not opened: ", fileName.c_str());
	}
}

void GpuSyncTableProvider::Read(const string& fileName)
{
	ifstream ifs(fileName.c_str());
	if (ifs.is_open())
	{
		int nParts, nAdjs, nodeFrom, nodeTo, nSendBlocks, nRecvBlocks, _bodyId, _solverId;

		ifs >> nParts >> _bodyId >> _solverId >> nAdjs;
		for (int j = 0; j < nAdjs; j++)
		{
			ifs >> nodeFrom >> nodeTo >> nSendBlocks >> nRecvBlocks;
			string block;
			int offset, count;
			// sends
			for (int i = 0; i < nSendBlocks; i++)
			{
				ifs >> block;
				ParseBlock(block, offset, count);
				//
				if (nodeFrom == _rank)
				{
					vector<int> block = vector<int>();
					block.push_back(offset);
					block.push_back(count);
					_send.push_back(block);
				}
			}
			for (int i = 0; i < nRecvBlocks; i++)
			{
				ifs >> block;
				ParseBlock(block, offset, count);
				//				
				if (nodeTo == _rank)
				{
					vector<int> block = vector<int>();
					block.push_back(offset);
					block.push_back(count);
					_recieve.push_back(block);
				}
			}
		}
		ifs.close();
	}
	else
	{
		printf("File not opened: ", fileName.c_str());
	}
}