#pragma once

#include "SyncTable.h"

#include <pthread.h>
//#include <windows.h>


class PThreadsManager;

typedef  void* (*pThreadFunc)(void*);

struct PThreadData
{
	int _threadNumber;
	int _iterationsNumber;
	PThreadsManager* _manager;
	pThreadFunc _task;

	// TODO: сделать общий порядок для формальных параметров
	// и членов класса в списке инициализации
	PThreadData
		(
			int threadNumber,
			int iterationsNumber,
			pThreadFunc task,
			PThreadsManager* manager
		)
		:
			_threadNumber(threadNumber),
			_iterationsNumber(iterationsNumber),
			_manager(manager),
			_task(task)
	{
	}
};

struct BarrierData
{
	pthread_mutex_t _mutex_barrier;
	pthread_cond_t _cond_barrier;
	int _barrier_counter;
	
	BarrierData():
	_barrier_counter(0)
	{
		pthread_mutex_init(&_mutex_barrier, NULL);
		pthread_cond_init(&_cond_barrier, NULL);
	}

	~BarrierData()
	{
		pthread_mutex_destroy(&_mutex_barrier);
		pthread_cond_destroy(&_cond_barrier);	
	}
	
	void WaitFor(int threadsNumber)
	{
		pthread_mutex_lock(&_mutex_barrier);
		_barrier_counter++;
		if (_barrier_counter < threadsNumber) 
		{
			pthread_cond_wait(&_cond_barrier,&_mutex_barrier);
		}
		else 
		{ 
			_barrier_counter=0; 
			pthread_cond_broadcast(&_cond_barrier);
		}
		pthread_mutex_unlock(&_mutex_barrier);	
	}

};

/** Управление потоками
*/
class PThreadsManager
{
	// потоки
	vector<pthread_t> _threads;
	
	//данные потоков
	vector<PThreadData*> _threadsData;

	//барьеры
	vector<BarrierData*> _barriers;

	// число потоков
	int _threadsNumber;
	
	//int _created_threads;

	SyncTable* _table;

	// память для обмена
	vector<double *> sendBuffers;
	vector<double *> recvBuffers;
	
public:
	PThreadsManager(int num, SyncTable* table);
	
	void Pack(int id, double const*data);
	void Unpack(int id, double *data);

	void Wait();
	
	void AddTask(int threadNumber, int iterationsNumber, pThreadFunc task);

	void RunTask(int threadNumber);
	void AddBariers(int barriersNumber);

	void Barrier(int barrierId);
	void GlobalBarrier(int barrierId);
	void AllToAll();

	~PThreadsManager();	
};
