#include "ExchangePair.h"
#include "GetOffsets.h"

ExchangePair::ExchangePair(void)
{
}

ExchangePair::~ExchangePair(void)
{
}
void ExchangePair::AddSendOffset(int offset)
{
		_send.push_back(offset);
}
void ExchangePair::AddSendOffsets(vector<int> const&offsets)
{
	_send.insert(_send.end(), offsets.begin(), offsets.end());
}

void ExchangePair::AddSendOffsets(int const*offsets, int size)
{
	for (int i = 0; i < size; i++)
		_send.push_back(offsets[i]);
}

void ExchangePair::AddSendOffsets(int offset, int count)
{
	for (int i = offset; i < offset+count; i++)
		_send.push_back(i);
}

void ExchangePair::AppendSend(vector<int> &output)
{
	if (_send.size() > 0)
		output.insert(output.end(), _send.begin(), _send.end());
}
void ExchangePair::AppendRecv(vector<int> &output)
{
	if (_recv.size() > 0)
		output.insert(output.end(), _recv.begin(), _recv.end());
}
void ExchangePair::AddRecvOffsets(int const*offsets, int size)
{
	for (int i = 0; i < size; i++)
		_recv.push_back(offsets[i]);
}
void ExchangePair::AddRecvOffsets(vector<int> const&offsets)
{
	_recv.insert(_recv.end(), offsets.begin(), offsets.end());
}
void ExchangePair::AddRecvOffset(int offset)
{
	_recv.push_back(offset);
}

void ExchangePair::AddRecvOffsets(int offset, int count)
{
	for (int i = offset; i < offset+count; i++)
		_recv.push_back(i);
}
void ExchangePair::GetOffsets(vector<pair<int, int> > &blocksSend, 
	vector<pair<int, int> > &blocksRecv) const
{
		::GetOffsets(_send, blocksSend);
		::GetOffsets(_recv, blocksRecv);
}

void ExchangePair::ConvertTo
	(
		int freedomsCount,
		ExchangePair& addressesExchangePair
	)	const
{
	Addresses sendingAdresses;

	for
		(
			Addresses::const_iterator sendingAddressesIterator = _send.begin();
			sendingAddressesIterator != _send.end();
			++sendingAddressesIterator
		)
	{
		int sendingAddress = *sendingAddressesIterator;

		for (int freedomIndex = 0; freedomIndex < freedomsCount; freedomIndex++)
		{
			sendingAdresses.push_back(freedomsCount * sendingAddress + freedomIndex);
		}
	}
	addressesExchangePair.AddSendOffsets(sendingAdresses);

	Addresses receivingAdresses;

	for
		(
			Addresses::const_iterator receivingAddressesIterator = _recv.begin();
			receivingAddressesIterator != _recv.end();
			++receivingAddressesIterator
		)
	{
		int receivingAddress = *receivingAddressesIterator;

		for (int freedomIndex = 0; freedomIndex < freedomsCount; freedomIndex++)
		{
			receivingAdresses.push_back(freedomsCount * receivingAddress + freedomIndex);
		}
	}
	addressesExchangePair.AddRecvOffsets(receivingAdresses);
}