#include "GetOffsets.h"

void GetOffsets(const vector<int>& data, vector<pair<int, int> >& blocks)
{
	blocks.clear();
	int j = 0;
	do
	{
		int startId = data[j];
		int counter = 1;
		j++;
		while(j < data.size())
		{
			if((data[j]-data[j-1]) != 1)
			{
				//stopId = data[j-1];
				break;
			}
			j++;
			counter++;
		}
		blocks.push_back(pair<int,int>(startId,counter));
	}
	while(j < data.size());
}