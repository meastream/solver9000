#pragma once
#include "ExchangePair.h"
#include "mpi.h"
#include <algorithm>
#include <cstring>

using std::copy;

struct ExchangeBlock
{
	int offsetFrom; // смещение из send-буфера
	int offsetTo;	// смещение из recv-буфера
	int size;		// размер блока

	ExchangeBlock() {};
	ExchangeBlock(int from, int to, int size)
	{
		this->offsetFrom = from;
		this->offsetTo = to;
		this->size = size;
	}
};

/** Таблица синхронизации
*	хранит адреса для обмена данными при синхронизации между каждой парой процессов
*	осуществляет упаковку и распаковку данных при обмене
*	осуществляет локальное перераспределение данных
*
*	@author Victor Getmanskiy
*/
class SyncTable
{
	vector<ExchangeVector> _table;
	vector<vector<ExchangeBlock> > _offsetsTable;
public:
	void GetOffsets(int i, int j, 
		vector<pair<int, int> > &blocksSend, vector<pair<int, int> > &blocksRecv);

	int CountParts() const {return _table.size();}
	
	ExchangeVector& operator [] (int rowId)
	{
		return _table[rowId];
	}

	/** Конструктор по количеству процессов
	*/
	SyncTable(int nProcs);

	SyncTable(){};

	void Allocate(int nProcs);

	int CountSend(int rowId) const;
	int CountRecv(int rowId) const;

	int SendBlockSize(int i, int j) const
	{
		return _table[i][j].CountSend();
	}

	int SendBlockAddress(int i, int j, int k)
	{
		return _table[i][j].GetSend(k);
	}

	int RecvBlockSize(int i, int j) const
	{
		return _table[i][j].CountRecv();
	}

	int RecvBlockAddress(int i, int j, int k)
	{
		return _table[i][j].GetRecv(k);
	}

	/** Упаковка в буфер отправки
	* @param rowId - индекс строки (номер процесса-отправителя)
	* @param data - данные процесса-отправителя
	* @param buffer - буфер отправки
	*/
	void Pack(int rowId, const double *data, double* buffer) const
	{
		int offset = 0;
		for(ExchangeColCIterator it = _table[rowId].begin(); it !=_table[rowId].end(); ++it)
		{
			for(int i = 0; i < it->CountSend(); i++)
				buffer[offset++] = data[it->GetSend(i)];
		}
	}

	/** Распаковка из буфера приема
	* @param rowId - индекс строки (номер процесса-получателя)
	* @param data - данные процесса-получателя
	* @param buffer - буфер приема
	*/
	void Unpack(int rowId, double *data, const double* buffer) const
	{
		int offset = 0;
		for(ExchangeColCIterator it = _table[rowId].begin(); it !=_table[rowId].end(); ++it)
		{
			for(int i = 0; i < it->CountRecv(); i++)
				data[it->GetRecv(i)] = buffer[offset++];
		}
	}

	/** Кэширование смещений для ускорения обменна
	* выполнять только после формирования таблицы
	*/
	void CalculateOffsets()
	{

		for(int i = 0; i < _table.size(); i++)
		{
			_offsetsTable[i][0].offsetTo = 0;
			_offsetsTable[i][0].size = _table[i][0].CountRecv();

			_offsetsTable[0][i].offsetFrom = 0;

			for(int j = 1; j < _table.size(); j++)
			{
				_offsetsTable[i][j].size = _table[i][j].CountRecv();
				_offsetsTable[i][j].offsetTo = _offsetsTable[i][j-1].offsetTo+_offsetsTable[i][j-1].size;
				_offsetsTable[j][i].offsetFrom = _offsetsTable[j-1][i].offsetFrom+_table[j-1][i].CountRecv();
			}
		}
	}

	/** Перераспределение данных из буферов отправки в буферы приема
	*/
	void Exchange(const double * const * sendBuffers, double** recvBuffers)
	{		
		for(int i = 0; i < _table.size(); i++)
			for(int j = 0; j < _table.size(); j++)
			{
				ExchangeBlock block = _offsetsTable[i][j];
				if(block.size != 0)
					memcpy(recvBuffers[i]+block.offsetTo, sendBuffers[j]+block.offsetFrom, block.size*sizeof(double));
			}
	}

	/** Перераспределение данных из буферов отправки в буферы приема
	*/
	void Exchange(const vector<double*>& sendBuffers, vector<double*>& recvBuffers)
	{		
		for(int i = 0; i < _table.size(); i++)
			for(int j = 0; j < _table.size(); j++)
			{
				const ExchangeBlock& block = _offsetsTable[i][j];
				if(block.size != 0)
					memcpy(recvBuffers[i]+block.offsetTo, sendBuffers[j]+block.offsetFrom, block.size*sizeof(double));
			}
	}

	/** Перераспределение данных из буферов отправки в буферы приема
	*/
	void Exchange(const vector<vector<double> >& sendBuffers, vector<vector<double> >& recvBuffers)
	{		
		for(int i = 0; i < _table.size(); i++)
			for(int j = 0; j < _table.size(); j++)
			{
				ExchangeBlock block = _offsetsTable[i][j];
				if(block.size != 0)
				{
					copy(sendBuffers[j].begin()+block.offsetFrom, 
						sendBuffers[j].begin()+block.offsetFrom+block.size,
						recvBuffers[i].begin()+block.offsetTo);
				}
			}
	}

	void OneToAll
		(
			int subsolverId,
			const double* sendBuffer,
			vector<double*>& recvBuffers
		);

	void ConvertTo
		(
			int freedomsCount,
			int nodesCount,
			SyncTable& addressesSyncTable
		)	const;

	~SyncTable(void);

	int CountNonzeroBlocks() const;

	int* GetSendOffsets(int i, int j);
	int* GetRecvOffsets(int i, int j);
	int* GetSendLengths(int i, int j);
	int* GetRecvLengths(int i, int j);
	void CalculateMPIDataTypes(int rank, int size, int counts[4], int displs[4], MPI_Datatype sTypes[4], MPI_Datatype rTypes[4]);
};
