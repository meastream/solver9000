#include "MPISynchronisation.h"

void MPISynchronisation::AlltoallvSynchronize(int rank, int size, TestSyncTable syncTable, double* myData)
{
	double sBuf[1000], rBuf[1000];
	int counts[4], displs[4];

	syncTable.PackSendData(rank, myData, sBuf, displs, counts);

	MPI_Alltoallv(sBuf, counts, displs, MPI_DOUBLE, rBuf, counts, displs, MPI_DOUBLE, MPI_COMM_WORLD);

	syncTable.UnpackRecvData(rank, myData, rBuf, displs);
}

void MPISynchronisation::AlltoallwSynchronize(int rank, int size, TestSyncTable syncTable, double* myData)
{
	Table sc = syncTable._table;
	int counts[4], displs[4] = { 0 };
	MPI_Datatype sTypes[4], rTypes[4];

	for (int i = 0; i < size; i++)
	{
		// ���� ���� ���������� �����
		if (sc[rank][i].size() > 0)
		{
			// ������������ ��� ������ ��� ��������
			MPI_Datatype sendType;
			MPI_Type_indexed(sc[rank][i].size(), syncTable.GetCounts(rank, i), syncTable.GetFromOffsets(rank, i), MPI_DOUBLE, &sendType);
			MPI_Type_commit(&sendType);

			// ������������ ��� ������ ��� ������
			MPI_Datatype recvType;
			MPI_Type_indexed(sc[i][rank].size(), syncTable.GetCounts(i, rank), syncTable.GetToOffsets(i, rank), MPI_DOUBLE, &recvType);
			MPI_Type_commit(&recvType);

			// ��������� ���������
			counts[i] = 1;
			sTypes[i] = sendType;
			rTypes[i] = recvType;
		}
		else
		{
			// ��������� ���� �������
			counts[i] = 0;
			sTypes[i] = MPI_CHAR;
			rTypes[i] = MPI_CHAR;
		}
	}

	MPI_Alltoallw(myData, counts, displs, sTypes, myData, counts, displs, rTypes, MPI_COMM_WORLD);
}

void MPISynchronisation::SendrecvSynchronize(int rank, int size, TestSyncTable syncTable, double* myData)
{
	Table sc = syncTable._table;

	for (int i = 0; i < size; i++)
	{
		// ���� ���� ���������� �����
		if (sc[rank][i].size() > 0)
		{
			// ������������ ��� ������ ��� ��������
			MPI_Datatype sendType;
			MPI_Type_indexed(sc[rank][i].size(), syncTable.GetCounts(rank, i), syncTable.GetFromOffsets(rank, i), MPI_DOUBLE, &sendType);
			MPI_Type_commit(&sendType);

			// ������������ ��� ������ ��� ������
			MPI_Datatype recvType;
			MPI_Type_indexed(sc[i][rank].size(), syncTable.GetCounts(i, rank), syncTable.GetToOffsets(i, rank), MPI_DOUBLE, &recvType);
			MPI_Type_commit(&recvType);

			// ������������ �������
			MPI_Status status;
			MPI_Sendrecv(myData, 1, sendType, i, 0, myData, 1, recvType, i, 0, MPI_COMM_WORLD, &status);
		}
	}
}

void MPISynchronisation::SendRecvSynchronize(int rank, int size, TestSyncTable syncTable, double* myData)
{
	Table sc = syncTable._table;

	for (int i = 0; i < size; i++)
	{
		// ���� ���� ���������� �����
		if (sc[rank][i].size() > 0)
		{
			// ������������ ��� ������ ��� ��������
			MPI_Datatype sendType;
			MPI_Type_indexed(sc[rank][i].size(), syncTable.GetCounts(rank, i), syncTable.GetFromOffsets(rank, i), MPI_DOUBLE, &sendType);
			MPI_Type_commit(&sendType);

			// ������������ ��� ������ ��� ������
			MPI_Datatype recvType;
			MPI_Type_indexed(sc[i][rank].size(), syncTable.GetCounts(i, rank), syncTable.GetToOffsets(i, rank), MPI_DOUBLE, &recvType);
			MPI_Type_commit(&recvType);

			// ���������� ������ 
			MPI_Send(myData, 1, sendType, i, 0, MPI_COMM_WORLD);

			// ��������� ������ 
			MPI_Status status;
			MPI_Recv(myData, 1, recvType, i, 0, MPI_COMM_WORLD, &status);

		}
	}
}

