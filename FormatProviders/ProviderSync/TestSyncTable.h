#pragma once
#include "ExchangePair.h"
#include "SyncTable.h"
#include <vector>
using namespace std;

typedef vector<vector<vector<ExchangeBlock>>> Table;

class TestSyncTable
{
public:
	Table _table;

	TestSyncTable(int countSubmodels) 
	{ 
		//_table = SmallTestInit(countSubmodels);
		_table = BigTestInit(countSubmodels);
	}
	Table SmallTestInit(int countSubmodels);
	Table BigTestInit(int countSubmodels);

	void PackSendData(int proc, double* data, double* pack, int* displs, int* counts);
	void UnpackRecvData(int proc, double* data, double* pack, int* displs);

	int* GetFromOffsets(int i, int j);
	int* GetToOffsets(int i, int j);
	int* GetCounts(int i, int j);
};
