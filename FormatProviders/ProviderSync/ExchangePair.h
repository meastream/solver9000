#pragma once
#include <vector>

using std::pair;
using std::vector;

typedef vector<int> Addresses;

class ExchangePair
{
Addresses _send;
Addresses _recv;
public:
	int CountRecv() const {return _recv.size();}
	int CountSend() const {return _send.size();}
	int GetSend(int id) const {return _send[id];}
	int GetRecv(int id) const{ return _recv[id];}
	
	void AddSendOffset(int offset);
	void AddSendOffsets(vector<int> const&offsets);
	void AddSendOffsets(int const*offsets, int size);
	void AddSendOffsets(int offset, int count);
	void AppendSend(vector<int> &output);
	
	void AddRecvOffset(int offset);
	void AddRecvOffsets(vector<int> const&offsets);
	void AddRecvOffsets(int const*offsets, int size);
	void AddRecvOffsets(int offset, int count);
	void AppendRecv(vector<int> &output);
	void GetOffsets(vector<pair<int, int> > &blocksSend, 
		vector<pair<int, int> > &blocksRecv) const;
	bool IsEmpty() const {return _send.size() == 0 && _recv.size() == 0;}
	void ConvertTo
		(
			int freedomsCount,
			ExchangePair& addressesExchangePair
		)	const;

	ExchangePair(void);
	~ExchangePair(void);
};

typedef vector<ExchangePair> ExchangeVector;
typedef vector<ExchangePair>::iterator ExchangeColIterator;
typedef vector<ExchangeVector>::iterator ExchangeRowIterator;
typedef vector<ExchangePair>::const_iterator ExchangeColCIterator;
typedef vector<ExchangeVector>::const_iterator ExchangeRowCIterator;
